package Test;
import static org.junit.Assert.*;

import org.junit.*;

import backend.Disease;
import backend.Tile;
import backend.Treatment;

public class TileTest {

	@Test
	public void testTile() {
		Tile t = new Tile(3, 4);

		assertEquals(3, t.getX());
		assertEquals(4, t.getY());
		assertFalse("No est� enfermo", t.getOnDisease());

		t.setX(6);
		t.setY(5);

		assertEquals(6, t.getX());
		assertEquals(5, t.getY());
	}

	@Test
	public void testAssignDisease() {
		Tile t = new Tile(3, 4);
		Disease d = new Disease(100, "enf");

		assertFalse("No est� enfermo", t.getOnDisease());

		t.setOnDisease(true);
		assertEquals(true, t.getOnDisease());

		assertFalse("No se enferma si ya est� enfermo", t.assignDisease(d, 100));
	}

	@Test
	public void testCure() {
		Tile t = new Tile(3, 4);
		Disease d = new Disease(100, "enf");
		Treatment c = new Treatment();
		c.setEffect("un efecto");

		d.asingTreatment(c);

		while (!t.getOnDisease()) {
			t.assignDisease(d, 100);
		}

		assertTrue("Est� enfermo", t.getOnDisease());

		t.cure(c);

		assertFalse("No est� enfermo luego de aplicar cura", t.getOnDisease());
	}

}
