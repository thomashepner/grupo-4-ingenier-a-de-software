package Test;
import static org.junit.Assert.*;

import org.junit.Test;

import backend.Disease;
import backend.Treatment;

public class DiseaseTest {

	@Test
	public void testAsingTreatment() {
		Disease d = new Disease(0.1, "Enfermedad");
		Treatment t = new Treatment();
		t.setEffect("cura");
		d.asingTreatment(t);

		assertEquals("cura", d.getCure().activateEffect());
	}

	@Test
	public void testIsTreatable() {
		Disease d = new Disease(0.1, "Enfermedad");
		Treatment t = new Treatment();
		t.setEffect("cura");
		d.asingTreatment(t);

		assertTrue(d.isTreatable(t));
		
		Treatment t1 = new Treatment();
		t1.setEffect("curars");
		assertFalse(d.isTreatable(t1));
	}
}
