package Test;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import backend.Disease;
import backend.Treatment;

public class TreatmentTest {

	@Test
	public void testGetEffectDuration() {
		Treatment t = new Treatment();

		t.setEffectDuration(10);

		assertNotNull(t.getEffectDuration());
		assertEquals(10, t.getEffectDuration());
	}

	@Test
	public void testGetAcquisitionTime() {
		Treatment t = new Treatment();

		t.setAcquisitionTime(10);

		assertNotNull(t.getAcquisitionTime());
		assertEquals(10, t.getAcquisitionTime());
	}

	@Test
	public void testGetDiseases() {
		Treatment t = new Treatment();
		ArrayList<Disease> ds = new ArrayList<Disease>();
		ds.add(new Disease(0.1, "1"));
		ds.add(new Disease(0.1, "2"));
		
		t.setDiseases(ds);
		
		assertEquals("1", t.getDiseases().get(0).getName());
		assertEquals("2", t.getDiseases().get(1).getName());
	}

	@Test
	public void testActivateEffect() {
		Treatment t = new Treatment();
		t.setEffect("Cura algo");
		
		assertEquals("Cura algo", t.activateEffect());
	}

}
