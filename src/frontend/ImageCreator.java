package frontend;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.colorchooser.*;

import java.io.BufferedInputStream;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;

import javax.imageio.ImageIO;

import backend.Game;

public class ImageCreator implements ActionListener {
	private JColorChooser jcc;
	private Image dot;
	private Image persona;
	private Color c;
	private Game miGame;
	private String diseaseName;
	private int probability;

	public Color getC() {
		return c;
	}

	public void setC(Color c) {
		this.c = c;
	}

	public ImageCreator(JColorChooser jcc, Image dot, Image persona,
			Game miGame, String diseaseName, int probability) {
		this.jcc = jcc;
		this.dot = dot;
		this.persona = persona;
		this.miGame = miGame;
		this.diseaseName = diseaseName;
		this.probability = probability;
	}
	public ImageCreator(Color c, Game migame, String diseaseName, int probability){
		this.c=c;
		this.miGame=migame;
		this.diseaseName=diseaseName;
		this.probability=probability;
		this.persona=migame.getGridFrontend().getImage((char)37);
		this.dot=migame.getGridFrontend().getImage((char)35);
				
		/*		.getGridFrontend().getImage((char) 35), game
				.getGridFrontend().getImage((char) 37), game,
				*/
		actionPerformed(null);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(jcc!=null)
			this.c = jcc.getColor();
		BufferedImage doto = colorImage(toBufferedImage(dot), c);
		BufferedImage person = colorImage(toBufferedImage(persona), c);
		try {
			ImageIO.write(doto, "png", new File("src\\Test.png"));
			ImageIO.write(person, "png", new File("src\\Test2.png"));
			InputStream im1 = new BufferedInputStream(new FileInputStream(
					"src\\Test.png"));
			InputStream im2 = new BufferedInputStream(new FileInputStream(
					"src\\Test2.png"));
			Image uno = ImageIO.read(im1);
			Image dos = ImageIO.read(im2);
			miGame.getGridFrontend().addImage(uno);
			miGame.getGridFrontend().addImage(dos);
			miGame.addNewDiseaseInGame(diseaseName, 4, probability, (char) (miGame
					.getGridFrontend().getMapeoActual() - 2), (char) (miGame
					.getGridFrontend().getMapeoActual() - 1));
			/*
			 * Me falta volver a leer estas imagenes y entregarla al
			 * gridfrontend
			 */
			File f1 = new File("src//Test.png");
			File f2 = new File("src//Test2.png");
			f1.delete();
			f2.delete();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static BufferedImage toBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage(img.getWidth(null),
				img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();

		// Return the buffered image
		return bimage;
	}

	private static BufferedImage colorImage(BufferedImage image, Color newC) {
		int width = image.getWidth();
		int height = image.getHeight();

		for (int xx = 0; xx < width; xx++) {
			for (int yy = 0; yy < height; yy++) {
				Color originalColor = new Color(image.getRGB(xx, yy), true);
				if (originalColor.getAlpha() != 0)
					image.setRGB(xx, yy, newC.getRGB());

			}
		}
		return image;
	}

}
