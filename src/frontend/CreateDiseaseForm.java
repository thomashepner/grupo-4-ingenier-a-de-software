package frontend;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

public class CreateDiseaseForm extends JPanel {
	private JTextField[] fields;

	public CreateDiseaseForm() {
		super(new BorderLayout());
		JPanel labelPanel = new JPanel(new GridLayout(5, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(5, 1));
		add(labelPanel, BorderLayout.WEST);
		add(fieldPanel, BorderLayout.CENTER);
		fields = new JTextField[5];
		// Nombre de la enfermedad
		fields[0] = new JTextField();
		fields[0].setToolTipText("Ingrese el nombre de la enfermedad");
		fields[0].setColumns(15);
		JLabel label0 = new JLabel("Nombre de la enfermedad", JLabel.RIGHT);
		label0.setLabelFor(fields[0]);
		labelPanel.add(label0);
		JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
		p.add(fields[0]);
		fieldPanel.add(p);
		// Probabilidad de infecci�n
		fields[1] = new JTextField();
		fields[1].setToolTipText("Ingrese la probabilidad de infecci�n");
		fields[1].setColumns(3);
		JLabel label1 = new JLabel("Probabilidad de infecci�n (%)", JLabel.RIGHT);
		label1.setLabelFor(fields[1]);
		labelPanel.add(label1);
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		p1.add(fields[1]);
		fieldPanel.add(p1);
		//Cura
		fields[2] = new JTextField();
		fields[2].setToolTipText("Ingrese el nombre de la cura a esta enfermedad");
		fields[2].setColumns(15);
		JLabel label2 = new JLabel("Nombre de la cura", JLabel.RIGHT);
		label2.setLabelFor(fields[2]);
		labelPanel.add(label2);
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		p2.add(fields[2]);
		fieldPanel.add(p2);
		//Tiempo de adquisici�n en turnos
		fields[3] = new JTextField();
		fields[3].setToolTipText("Ingrese el tiempo inicial en turnos de adquisici�n de la enfermedad");
		fields[3].setColumns(3);
		JLabel label3 = new JLabel("Tiempo de adquisici�n", JLabel.RIGHT);
		label3.setLabelFor(fields[3]);
		labelPanel.add(label3);
		JPanel p3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		p3.add(fields[3]);
		fieldPanel.add(p3);
		//Tiempo de cura
		fields[4] = new JTextField();
		fields[4].setToolTipText("Ingrese el tiempo en turnos que se demora en curar a la enfermedad");
		fields[4].setColumns(3);
		JLabel label4 = new JLabel("Tiempo de cura", JLabel.RIGHT);
		label4.setLabelFor(fields[4]);
		labelPanel.add(label4);
		JPanel p4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		p4.add(fields[4]);
		fieldPanel.add(p4);		
	}

	public String getText(int i) {
		return (fields[i].getText());
	}

}
