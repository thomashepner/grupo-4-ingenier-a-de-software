package frontend;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import backend.Disease;
import backend.EntityXmlHandler;
import backend.Game;

public class SendEntityForm {
	private JList list, worldList;
	private JScrollPane diseaseList;
	private final JFrame frame;
	private Disease selectedDisease;
	private ArrayList<Disease> diseasesInGame;
	private String[] worlds;
	private String selectedWorld;
	private Game game;

	public SendEntityForm(final ArrayList<Disease> diseasesInGame,
			final Game game) {
		this.diseasesInGame = diseasesInGame;
		this.game = game;
		String[] diseases = new String[diseasesInGame.size()];
		for (int i = 0; i < diseases.length; i++) {
			diseases[i] = diseasesInGame.get(i).getName();
		}
		this.list = new JList(diseases);
		this.diseaseList = new JScrollPane(list);
		String[] worlds = { "Mundo 1", "Mundo 2", "Mundo 3", "Mundo 4", "Mundo 5",
				"Mundo 6", "Mundo 7", "Mundo 8", "Mundo 9", "Mundo 10" };
		this.frame = new JFrame("Seleccionar Mundo");
		this.worlds = worlds;
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.worldList = new JList(worlds);
		JScrollPane worldScrollPane = new JScrollPane(worldList);

		Container contentPane = frame.getContentPane();
		contentPane.add(worldScrollPane, BorderLayout.NORTH);
		contentPane.add(diseaseList, BorderLayout.CENTER);
		frame.setLocationRelativeTo(null);
		frame.setSize(100, 300);
		frame.setVisible(true);

		final JButton submit = new JButton("Enviar");
		frame.getContentPane().add(submit, BorderLayout.SOUTH);

		getSelectedDisease();
		getSelectedWorld();

		submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (checkCompletion()) {
					game.pause();
					createAndSend();
					int a = 4;
				}

			}
		});
	}

	public void diseaseSentDialog() {
		final JFrame frame = new JFrame("Enfermedad enviada");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JLabel label = new JLabel("Has enviado la enfermedad "
				+ this.selectedDisease.getName() + " al " + this.selectedWorld
				+ ". �Adi�s, " + this.selectedDisease.getName() + "!");
		frame.add(label, BorderLayout.CENTER);
		JButton ok = new JButton("OK");
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		frame.add(ok, BorderLayout.SOUTH);
		frame.setSize(380, 100);
		frame.setVisible(true);

	}



	public void createAndSend() {
		EntityXmlHandler xml = new EntityXmlHandler();
		// System.out.println(xml.buildXmlString(this.selectedDisease));
		send(xml.buildXmlString(this.selectedDisease));
	}

	public void send(String string) {
		Socket socket = null;
		DataOutputStream salida = null;
		try {
			switch (this.selectedWorld) {
			case "Mundo 1":
				socket = new Socket("146.155.115.201", 6969);
				break;
			case "Mundo 2":
				socket = new Socket("146.155.115.202", 1337);
				break;
			case "Mundo 3":
				socket = new Socket("146.155.115.203", 4444);
				break;
			case "Mundo 4":
				socket = new Socket("146.155.115.204", 8080);
				break;
			case "Mundo 5":
				socket = new Socket("146.155.115.215", 4444);
				break;
			case "Mundo 6":
				socket = new Socket("146.155.115.206", 1234);
				break;
			case "Mundo 7":
				socket = new Socket("146.155.115.207", 1234);
				break;
			case "Mundo 8":
				socket = new Socket("146.155.115.218", 1234);
				break;
			case "Mundo 9":
				socket = new Socket("146.155.115.209", 4444);
				break;
			case "Mundo 10":
				socket = new Socket("146.155.115.220", 1234);
				break;
			default:
				break;
			}
			salida = new DataOutputStream(socket.getOutputStream());
			salida.writeBytes(string);
			socket.close();
			selectedDisease.erase();
			this.game.resume();

		} catch (UnknownHostException e) {
			System.err.println("Don't know about host");
			showConectionErrorDialog(
					"No conocemos ese mundo, int�ntalo m�s tarde.");
			this.frame.dispose();
			this.game.resume();
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection");
			showConectionErrorDialog(
					"No logramos conectar con otro mundo. Por favor, int�ntalo m�s tarde.");
			this.frame.dispose();
			this.game.resume();
		}
	}

	public void showConectionErrorDialog(String error) {
		final JFrame frame = new JFrame("Error de conexi�n");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JLabel label = new JLabel(error);
		frame.add(label, BorderLayout.CENTER);
		JButton ok = new JButton("OK");
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		frame.add(ok, BorderLayout.SOUTH);
		frame.setSize(500, 100);
		frame.setVisible(true);
	}

	public void getSelectedDisease() {
		MouseListener mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseEvent) {
				JList theList = (JList) mouseEvent.getSource();
				if (mouseEvent.getClickCount() == 1) {
					int index = theList.locationToIndex(mouseEvent.getPoint());
					if (index >= 0) {
						Object o = theList.getModel().getElementAt(index);
						System.out.println("Clicked on: " + o.toString());
						checkSelectedDisease(o.toString());
					}
				}

			}
		};
		this.list.addMouseListener(mouseListener);
	}

	public void getSelectedWorld() {
		MouseListener mouseListener = new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseEvent) {
				JList theList = (JList) mouseEvent.getSource();
				if (mouseEvent.getClickCount() == 1) {
					int index = theList.locationToIndex(mouseEvent.getPoint());
					if (index >= 0) {
						Object o = theList.getModel().getElementAt(index);
						System.out.println("Clicked on: " + o.toString());
						checkSelectedWorld(o.toString());
					}
				}

			}
		};
		this.worldList.addMouseListener(mouseListener);
	}

	public void checkSelectedDisease(String selectedLabel) {
		for (int i = 0; i < this.diseasesInGame.size(); i++) {
			if (diseasesInGame.get(i).getName() == selectedLabel) {
				this.selectedDisease = diseasesInGame.get(i);
				break;
			}
		}
	}

	public void checkSelectedWorld(String selectedLabel) {
		for (int i = 0; i < this.worlds.length; i++) {
			if (this.worlds[i] == selectedLabel) {
				this.selectedWorld = this.worlds[i];
				break;
			}
		}
	}

	public boolean checkCompletion() {
		if (this.selectedDisease == null) {
			JFrame error = new JFrame("Error");
			error.setLocationRelativeTo(null);
			JLabel label = new JLabel(
					"No seleccionaste ninguna entidad para enviar");
			error.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			error.add(label, BorderLayout.CENTER);
			error.setSize(300, 200);
			error.setVisible(true);
			return false;

		}
		if (this.selectedWorld == null) {
			JFrame error = new JFrame("Error");
			JLabel label = new JLabel(
					"No seleccionaste ninguna entidad para enviar");
			error.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			error.setLocationRelativeTo(null);
			error.add(label, BorderLayout.CENTER);
			error.setSize(100, 200);
			error.setVisible(true);
			return false;
		}
		return true;
	}

}
