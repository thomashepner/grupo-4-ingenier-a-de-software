package frontend;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import backend.Disease;
import backend.EntityXmlHandler;
import backend.Game;
import backend.User;

import java.awt.*;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Random;



public class MainWindow extends JFrame implements ActionListener {

	final int X_Margin = 27;
	final int Y_Margin = 64;

	int coordX = -1;
	int coordY = -1;

	class guardarAux
	{
	    public Color c; 
	    public String DiseaseName;  
	    public int chance; 
	 };
	 ArrayList<guardarAux> begin;
	private JMenuBar menuBar;

	GridFrontend frontend;
	JPanel statePanel;
	Timer timer;

	Game game;
	User u;

	final JLabel picLabel;

	/**
	 * Constructor
	 */
	public MainWindow() {
		u = new User();
		game = new Game();

		BufferedImage myPicture = null;
		try {
			myPicture = ImageIO
					.read(this.getClass().getResource("..\\portada.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		picLabel = new JLabel(new ImageIcon(myPicture));
		add(picLabel);

		setSize(myPicture.getWidth(), myPicture.getHeight() + Y_Margin);
		begin = new ArrayList<guardarAux>();
		initUI();
	}

	/**
	 * Inicializa todos los componentes de la Barra de men�
	 */
	private void initUI() {
		this.menuBar = new JMenuBar();

		JMenu login = new JMenu("Login");
		menuBar.add(login);
		JMenuItem viewconect = createConectMenuItem(menuBar);
		login.add(viewconect);

		JMenu juego = new JMenu("Juego");
		menuBar.add(juego);
		JMenuItem newGame = createNewGameMenuItem(menuBar);
		JMenuItem exit = createEMenuItem();
		juego.add(newGame);
		juego.add(exit);

		JMenu enfermedad = new JMenu("Enfermedad");
		menuBar.add(enfermedad);
		JMenuItem createDisease = createCreateDiseaseMenuItem();
		JMenuItem openDisease = createOpenDiseaseMenuItem();
		enfermedad.add(createDisease);
		enfermedad.add(openDisease);

		JMenu cura = new JMenu("Cura");
		menuBar.add(cura);
		JMenuItem applyCure = createApplyCureMenuItem();
		cura.add(applyCure);

		JMenu view = new JMenu("Ver");
		menuBar.add(view);
		JMenuItem viewRanking = createViewRankingMenuItem();
		JMenuItem viewMyScores = createViewMyScoresMenuItem();
		view.add(viewRanking);
		view.add(viewMyScores);

		setJMenuBar(menuBar);

		setTitle("LifeDisease");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Deshabilitar men�es que tienen que ver con el desarrollo del juego
		// mientras no haya juego 'corriendo'
		for (int itemID = 0; itemID < menuBar.getMenuCount(); itemID++) {
			if ((menuBar.getMenu(itemID) == enfermedad)) {
				menuBar.getMenu(itemID).setEnabled(false);
			} else if ((menuBar.getMenu(itemID) == cura)) {
				menuBar.getMenu(itemID).setEnabled(false);
			} else if ((menuBar.getMenu(itemID) == juego)) {
				menuBar.getMenu(itemID).setEnabled(false);
			} else if ((menuBar.getMenu(itemID) == view)) {
				menuBar.getMenu(itemID).setEnabled(false);
			}
		}
	}

	public void listenConection() throws Exception {

		ServerSocket serverSocket = new ServerSocket(8080);
		serverSocket.setSoTimeout(10000);
		Document xmlDocument = null;
		//while (true) {
			try {
				System.out.println("Esperando entidades en el puerto "
						+ serverSocket.getLocalPort() + "...");
				Socket server = serverSocket.accept();
				System.out.println("Conectado a  "
						+ server.getRemoteSocketAddress());
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(server.getInputStream()));
				String message = reader.readLine();
				System.out.println(message);
				String newLine = "";
				while (newLine != null) {
					message += newLine;
					newLine = reader.readLine();
				}

				DocumentBuilderFactory factory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				InputSource source = new InputSource();
				source.setCharacterStream(new StringReader(message));

				xmlDocument = builder.parse(source);
				EntityXmlHandler handler = new EntityXmlHandler();

				String diseaseName = "ForeignEntity";
				int length = handler.getHeight(xmlDocument)
						* handler.getWidth(xmlDocument);
				Random random = new Random();
				int chance = random.nextInt(16); // Probabilidad de contagio
													// m�xima de un 15%
				this.game.addNewDiseaseInGame(diseaseName, length, chance,
						(char) 35, (char) 37);

				server.close();
			} catch (SocketTimeoutException s) {
				System.out.println("Socket timed out!");
				//break;
			} catch (IOException e) {
				e.printStackTrace();
				//break;
			}
		//}
	}

	public void createConection(ArrayList<Disease> diseaseList) {
		SendEntityForm form = new SendEntityForm(diseaseList,game);

	}

	// METODOS DE CREACION DE JMENUITEMS:

	private JMenuItem createConectMenuItem(JMenuBar mainMenu) {
		JMenuItem conect = new JMenuItem("Conectar");
		final JMenuBar menuBar = mainMenu;

		conect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				GetUserPassword(menuBar);
			}
		});
		conect.setToolTipText("Comenzar nueva partida");

		return conect;
	}

	/**
	 * Crea menu item para Iniciar juego nuevo
	 */
	private JMenuItem createNewGameMenuItem(JMenuBar mainMenu) {
		JMenuItem newGame = new JMenuItem("Nueva partida");
		final JMenuBar menuBar = mainMenu;

		newGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				startNewGameForm(menuBar);
			}
		});
		newGame.setToolTipText("Comenzar nueva partida");

		return newGame;
	}

	/**
	 * Crea menu item para Cerrar
	 */
	private JMenuItem createEMenuItem() {
		JMenuItem eMenuItem = new JMenuItem("Salir");
		eMenuItem.setToolTipText("Salir de la aplicaci�n");
		eMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				System.exit(0);
			}
		});
		return eMenuItem;
	}

	/**
	 * Crea menu item para Crear Enfermedad
	 */
	private JMenuItem createCreateDiseaseMenuItem() {
		JMenuItem createDisease = new JMenuItem("Crear enfermedad");
		createDisease
				.setToolTipText("Crear una nueva enfermedad para el juego");

		createDisease.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				createDiseaseForm(false, null);
			}
		});

		return createDisease;
	}

	/**
	 * Crea menu item para Abrir Enfermedad
	 */
	private JMenuItem createOpenDiseaseMenuItem() {
		JMenuItem openDisease = new JMenuItem("Abrir enfermedad");
		openDisease
				.setToolTipText("Buscar un archivo e ingresar una enfermedad existente al juego");

		return openDisease;
	}

	/**
	 * Crea menu item para Aplicar una Cura
	 */
	private JMenuItem createApplyCureMenuItem() {
		JMenuItem applyCure = new JMenuItem("Aplicar cura");

		applyCure.setToolTipText("Aplicar cura a una persona del tablero");

		// Evento gatillado al aplicar cura:
		applyCure.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				applyCureForm();
			}
		});

		return applyCure;
	}

	/**
	 * Crea menu item para ver Ranking
	 */
	private JMenuItem createViewRankingMenuItem() {
		JMenuItem viewRanking = new JMenuItem("Ver ranking");
		viewRanking.setToolTipText("Ver posici�n en el ranking de jugadores");

		viewRanking.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Conseguir todos los usuarios y sus puntajes de la base de
				// datos...
				showGlobalRankingForm();
			}
		});

		return viewRanking;
	}

	/**
	 * Crea menu item para ver los puntajes propios
	 */
	private JMenuItem createViewMyScoresMenuItem() {
		JMenuItem viewMyScores = new JMenuItem("Ver puntajes");
		viewMyScores.setToolTipText("Ver mis puntajes de todas mis partidas");

		viewMyScores.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// Conseguir todos los puntajes del usuario actual y mostrar
				showLocalRankingForm();
			}
		});
		return viewMyScores;
	}

	// METODOS DE CREACION DE FORMULARIOS:

	/**
	 * Formulario de ingreso de usuarios
	 * */
	private void GetUserPassword(JMenuBar menuBar) {

		final JMenuBar barra = menuBar;

		final JFrame frame = new JFrame("Usuario");
		frame.setSize(200, 130);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frame.setLayout(new FlowLayout());

		JLabel label1 = new JLabel();
		label1.setText("Usuario:");
		frame.add(label1);

		final JTextField user = new JTextField();
		user.setPreferredSize(new Dimension(80, 20));
		user.setText("farojos");
		frame.add(user);

		JLabel label2 = new JLabel();
		label2.setText("Contrase�a:");
		frame.add(label2);

		final JTextField pass = new JTextField();
		pass.setPreferredSize(new Dimension(80, 20));
		pass.setText("lepassword");
		frame.add(pass);

		JButton submit = new JButton();
		submit.setText("Ingresar");
		frame.add(submit);

		submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {

				boolean pormientras = true;

				pormientras = u.canConnect(user.getText(), pass.getText());
				if (pormientras) {
					enableBarElementPlay(barra);

					frame.dispose();
				} else {
					showErrorDialog("Usuario o contrase�a invalido");
				}
			}
		});

		frame.setVisible(true);
	}

	/**
	 * Crea el form que se usa para crear enfermedades
	 */
	private void createDiseaseForm(boolean inicio, JPanel panell) {
		game.pause();

		final CreateDiseaseForm form = new CreateDiseaseForm();

		final boolean Inicio = inicio;
		final JPanel pan = panell;

		final JFrame frame = new JFrame("Crear nueva enfermedad");
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				game.resume();
			}
		});
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().add(form, BorderLayout.NORTH);

		JPanel panel = new JPanel();
		JButton submit = new JButton("Aceptar");
		panel.add(submit);
		final MainWindow auxiliar=this;
		submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Hacer cosas con los datos
				String diseaseName = form.getText(0);
				int probEnfermarPorciento = 0;
				if (tryParseInt(form.getText(1))) {
					probEnfermarPorciento = Integer.parseInt(form.getText(1));

					// double infectionChance =
					// Double.parseDouble(form.getText(1));
					if(game.getGridFrontend()!=null){
					SelectDiseaseColor sel = new SelectDiseaseColor(game
							.getGridFrontend().getImage((char) 35), game
							.getGridFrontend().getImage((char) 37), game,
							diseaseName, probEnfermarPorciento);
					}
					else{
						SelectDiseaseColor sel = new SelectDiseaseColor(diseaseName, probEnfermarPorciento, auxiliar, pan);
					}

					// game.addNewDiseaseInGame(diseaseName, 4);

					frame.dispose();
/*
					if (Inicio) {
						addDiseasesToPanel(pan);
					}
					*/
				}
			}
		});
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
		frame.pack();
		frame.setVisible(true);
	}
	public void addPerson(Color c, String diseaseName, int prob){
		guardarAux g = new guardarAux();
		g.c=c;
		g.DiseaseName=diseaseName;
		g.chance=prob;
		this.begin.add(g);
	}

	/**
	 * Crea el form que se usa para aplicar curas
	 */
	private void applyCureForm() {
		game.pause();

		// Se pedir�n las coordenadas de la persona a la que se sanar�, esto
		// puede ser haciendo click en el grid. Por ahora es ingresando
		// coordenadas:
		final JFrame frame = new JFrame("Aplicar Cura");
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				game.resume();
			}
		});
		frame.setSize(300, 150);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frame.setLayout(new FlowLayout());

		// Primera opci�n de input:
		JLabel label1 = new JLabel();
		label1.setText("Seleccionar cura");
		frame.add(label1);

		// Se inicializan las curas:
		String[] curas = game.getAvailableCures();
		final JComboBox curasComboBox = new JComboBox(curas);
		curasComboBox.setSelectedIndex(0);
		frame.add(curasComboBox);

		// Segunda opci�n de input: la coordenada X de la persona
		JLabel label2 = new JLabel();
		label2.setText("Coordenada X");
		frame.add(label2);

		final JTextField xCoord = new JTextField();
		xCoord.setText("coord. X de la persona");
		frame.add(xCoord);

		// Tercera opci�n de input: la coordenada Y de la persona
		JLabel label3 = new JLabel();
		label3.setText("Coordenada Y");
		frame.add(label3);

		final JTextField yCoord = new JTextField();
		yCoord.setText("coord. Y de la persona");
		frame.add(yCoord);

		JButton select = new JButton();
		select.setText("Seleccionar con click");
		frame.add(select);
		select.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				frame.setState(Frame.ICONIFIED);
				frontend.addMouseListener(new MouseAdapter() {
					public void mouseClicked(MouseEvent evt) {
						int coordX = frontend.getPositionX(evt);
						int coordY = frontend.getPositionY(evt);
						frame.setState(Frame.NORMAL);
						xCoord.setText(coordX + "");
						yCoord.setText(coordY + "");
						frontend.removeMouseListener(this);
					}
				});
			}
		});

		JButton submit = new JButton();
		submit.setText("Aplicar ( Costo $10 )");
		frame.add(submit);
		submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				if (curarSeleccionado(xCoord.getText(), yCoord.getText(),
						curasComboBox.getSelectedItem().toString())) {
					frame.dispose();
					game.resume();
				}
			}
		});

		frame.setVisible(true);
	}

	/**
	 * Cura a la persona en la posici�n (x,y)
	 */
	private boolean curarSeleccionado(String x, String y, String diseaseName) {
		if (tryParseInt(x) && tryParseInt(y)) {
			int xValue = Integer.parseInt(x);
			int yValue = Integer.parseInt(y);
			if (game.cure(xValue, yValue, diseaseName)) {
				return true;
			} else {
				showErrorDialog("Curaci�n inv�lida");
				return false;
			}

		} else {
			showErrorDialog("Coordenadas inv�lidas");
			return false;
		}
	}

	/**
	 * Crea el form que se usa para comenzar un juego nuevo
	 */
	private void startNewGameForm(JMenuBar menuBar) {
		final JMenuBar barra = menuBar;

		final JFrame frame = new JFrame("Juego Nuevo");
		frame.setSize(310, 290);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frame.setLayout(new FlowLayout());

		JLabel label = new JLabel();
		label.setText("Considerar que Ancho*Alto = n�mero de personas");
		frame.add(label);

		JLabel label1 = new JLabel();
		label1.setText("Ancho");
		frame.add(label1);

		final JTextField ancho = new JTextField();
		ancho.setPreferredSize(new Dimension(50, 20));
		ancho.setText("25");
		frame.add(ancho);

		JLabel label2 = new JLabel();
		label2.setText("Alto");
		frame.add(label2);

		final JTextField alto = new JTextField();
		alto.setPreferredSize(new Dimension(50, 20));
		alto.setText("20");
		frame.add(alto);

		final JPanel jPanel1 = new JPanel();
		jPanel1.setLayout(new GridLayout(0, 1));
		jPanel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		JScrollPane scrollPane = new JScrollPane(jPanel1,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setPreferredSize(new Dimension(150, 150));
		addDiseasesToPanel(jPanel1);

		frame.getContentPane().add(scrollPane);

		JButton submit = new JButton();
		submit.setText("Comenzar");
		frame.add(submit);
		submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				// boolean success = validarNewGameParameters();
				boolean pormientras = true;
				if (pormientras) {// ser� success
					enableBarElements(barra);
					addConectionBarElement();
					frame.dispose();
					beginGame(Integer.parseInt(ancho.getText()),
							Integer.parseInt(alto.getText()));
					for(int i = 0 ; i < begin.size(); i++){
						guardarAux u =begin.get(i);
						ImageCreator im = new ImageCreator(u.c,game,u.DiseaseName,u.chance);
					}
					//Crear enfermedades :B
				} else {
					showErrorDialog("Hay al menos un valor inv�lido");
				}
			}
		});

		JButton submitNewDisease = new JButton();
		submitNewDisease.setText("Crear Nueva Enfermedad");
		frame.add(submitNewDisease);
		submitNewDisease.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				createDiseaseForm(true, jPanel1);
			}
		});
		frame.setVisible(true);
	}

	public void addDiseasesToPanel(JPanel jpanel) {
		int y = jpanel.getPreferredSize().height;
		int x = jpanel.getPreferredSize().width;
		jpanel.removeAll();
		ArrayList<Disease> diseases = game.getDiseases();
		for (int indexD = 0; indexD < diseases.size(); indexD++) {
			Label cb = new Label("Enfermedad " + diseases.get(indexD).getName());
			jpanel.add(cb);
		}
		for(int i = 0 ; i < begin.size(); i++){
			guardarAux u =begin.get(i);
			Label cb = new Label("Enfermedad " + u.DiseaseName);
			jpanel.add(cb);
		}
		jpanel.setPreferredSize(new Dimension(x,y));
		jpanel.updateUI();
	}

	public void addConectionBarElement() {
		// Men� de env�o de entidades.
		JMenu conection = new JMenu("Conexi�n");
		menuBar.add(conection);
		JMenuItem createConection = new JMenuItem("Enviar Entidad");
		createConection
				.setToolTipText("Intenta enviar una entidad a otro mundo");

		createConection.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				createConection(game.getDiseases());
			}
		});
		conection.add(createConection);
		// Men� de recepci�n de nuevas entidades
		JMenuItem listenConection = new JMenuItem("Recibir entidades");
		listenConection
				.setToolTipText("Se pone en espera de nuevas entidades de otros mundos");

		listenConection.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					listenConection();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		conection.add(listenConection);
	}

	/**
	 * Hace comenzar el thread del juego nuevo
	 */
	public void beginGame(final int width, final int height) {
		game.setMapDimentions(width, height);
		game.setInitialDiseases();// Agregar al Game todas las enfermedades
									// iniciales
		frontend = game.getGridFrontend();
		statePanel = new JPanel();

		this.remove(picLabel);
		this.getContentPane().setLayout(new GridBagLayout());

		this.getContentPane().add(frontend.getFrontend());
		this.getContentPane().add(statePanel);

		statePanel.setPreferredSize(new Dimension(250, 300));
		generateStatePanel();

		resize();
		game.init();

		timer = new Timer(500, this);
		timer.start();

		frontend.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				coordX = frontend.getPositionX(evt);
				coordY = frontend.getPositionY(evt);
				System.out.println("x: " + coordX + "  y: " + coordY);
				selectedDisease(coordX, coordY);
				generateStatePanel();
			}
		});
	}

	public void actionPerformed(ActionEvent e) {
		generateStatePanel();
	}

	public void generateStatePanel() {

		statePanel.setLayout(new GridLayout(0, 1));
		Disease dis = game.getLastDiseaseSelected();
		JLabel stateLabel;
		statePanel.removeAll();

		if (game.isGameover()) {
			stateLabel = new JLabel("---------------------------------");
			statePanel.add(stateLabel);
			stateLabel = new JLabel("GAME OVER");
			statePanel.add(stateLabel);
			stateLabel = new JLabel("---------------------------------");
			statePanel.add(stateLabel);
			stateLabel = new JLabel("Usuario: " + u.getUsuario());
			statePanel.add(stateLabel);
			if (!u.scoreIsSaved())
				this.u.addPuntaje(u.getUsuario(), game.getPuntaje().getScore());
			timer.stop();
		} if (dis == null) {

			stateLabel = new JLabel("Puntaje:  " + game.getPuntaje().getScore());
			statePanel.add(stateLabel);
			stateLabel = new JLabel("Dias transcurridos:  " + game.getContador());
			statePanel.add(stateLabel);
			stateLabel = new JLabel("Dinero:  " + game.getPuntaje().getPuntaje());
			statePanel.add(stateLabel);
			stateLabel = new JLabel("---------------------------------");
			statePanel.add(stateLabel);
			if (coordX == -1 && coordY == -1) {
				stateLabel = new JLabel("No hay enfermedad seleccionada");
				statePanel.add(stateLabel);
			} else {
				for (int i = 0; i < game.getGrid().getTile(coordX, coordY)
						.showState().length; i++) {
					stateLabel = new JLabel(game.getGrid()
							.getTile(coordX, coordY).showState()[i]);
					statePanel.add(stateLabel);
				}

			}

			stateLabel = new JLabel("---------------------------------");
			statePanel.add(stateLabel);
			
			JButton curarOtro = new JButton();
			curarOtro.setText("Curar otro");
			statePanel.add(curarOtro);
			statePanel.updateUI();
			curarOtro.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					applyCureForm();

				}
			});
			if(coordX<0||coordY<0)
			{
				statePanel.updateUI();
				return;
			}
			if(!game.getGrid().getTile(coordX, coordY).getOnDisease()|| game.isGameover())
			{
				statePanel.updateUI();
				return;	
			}
			JButton curar = new JButton();
			curar.setText("Curar Seleccionado (costo: $10)");
			statePanel.add(curar);
			curar.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					game.pause();
					if (curarSeleccionado(""+coordX, ""+coordY, "Cura de "+game.getGrid().getTile(coordX, coordY).getDisease().getName())) {
						//System.out.println("curado");
					}
					game.resume();
					
				}
			});
				
			

		} else {

			stateLabel = new JLabel("Puntaje:  " + game.getPuntaje().getScore());
			statePanel.add(stateLabel);
			stateLabel = new JLabel("Dias transcurridos:  " + game.getContador());
			statePanel.add(stateLabel);
			stateLabel = new JLabel("Dinero:  $" + game.getPuntaje().getPuntaje());
			statePanel.add(stateLabel);
			stateLabel = new JLabel("---------------------------------");
			statePanel.add(stateLabel);
			int length = dis.ShowDiseaseState().length;
			for (int i = 0; i < length; i++) {
				stateLabel = new JLabel(dis.ShowDiseaseState()[i]);
				statePanel.add(stateLabel);
			}
			stateLabel = new JLabel("---------------------------------");
			statePanel.add(stateLabel);
			
			JButton curarOtro = new JButton();
			curarOtro.setText("Curar otro");
			statePanel.add(curarOtro);
			statePanel.updateUI();
			curarOtro.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent event) {
					applyCureForm();

				}
			});
		}
		
		
		
		statePanel.updateUI();
	}

	/**
	 * Crea el form que se usa para ver el ranking global de puntucaci�n
	 */
	private void showGlobalRankingForm() {
		game.pause();

		final JFrame frame = new JFrame("Puntajes Globales");
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				game.resume();
			}
		});
		frame.setSize(250, 350);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frame.setLayout(new FlowLayout());

		JLabel title = new JLabel();
		title.setText("         PUNTAJES GLOBALES         \n \n ");

		frame.add(title);

		// Mientras para los puntajes
		String puntajes[][] = u.topten();
		JLabel labelp[] = new JLabel[10];
		JLabel labels[] = new JLabel[10];

		for (int i = 0; i < 10; i++) {
			labelp[i] = new JLabel();
			labels[i] = new JLabel();
			labelp[i].setText(puntajes[i][0]);
			labels[i].setText(puntajes[i][1]);
			labelp[i].setPreferredSize(new Dimension(130, 15));
			labels[i].setPreferredSize(new Dimension(70, 15));
			frame.add(labelp[i]);
			frame.add(labels[i]);
		}

		JButton submit = new JButton();
		submit.setText("Cerrar");
		submit.setAlignmentX(CENTER_ALIGNMENT);
		submit.setAlignmentY(BOTTOM_ALIGNMENT);
		frame.add(submit);

		submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				frame.dispose();
			}
		});

		frame.setVisible(true);

	}

	/**
	 * Crea el form que se usa para ver los puntajes del usuario
	 */
	private void showLocalRankingForm() {
		game.pause();
		final JFrame frame = new JFrame("Puntajes Locales");
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				game.resume();
			}
		});
		frame.setSize(250, 350);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		frame.setLayout(new FlowLayout());

		JLabel title = new JLabel();
		title.setText("         PUNTAJES LOCALES         ");
		frame.add(title);

		// Mientras para los puntajes
		String puntajes[][] = u.toptenlocal();
		JLabel labelp[] = new JLabel[10];
		JLabel labels[] = new JLabel[10];

		for (int i = 0; i < 10; i++) {
			labelp[i] = new JLabel();
			labels[i] = new JLabel();
			labelp[i].setText(puntajes[i][0]);
			labels[i].setText(puntajes[i][1]);
			labelp[i].setPreferredSize(new Dimension(130, 15));
			labels[i].setPreferredSize(new Dimension(70, 15));
			frame.add(labelp[i]);
			frame.add(labels[i]);
		}

		JButton submit = new JButton();
		submit.setText("Cerrar");
		submit.setAlignmentX(CENTER_ALIGNMENT);
		submit.setAlignmentY(BOTTOM_ALIGNMENT);
		frame.add(submit);

		submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				frame.dispose();
			}
		});

		frame.setVisible(true);
	}

	// METODOS UTILES:
	/**
	 * Setea el tama�o de la mainWindow dependiendo del tama�o del juego nuevo.
	 */
	private void resize() {
		Dimension d = frontend.getFrontend().getPreferredSize();
		d.width += statePanel.getPreferredSize().width;
		d.width += X_Margin; // Borde
		d.height += Y_Margin; // Borde
		setSize(d);
		setLocationRelativeTo(null);
		repaint();
	}

	/**
	 * Muestra una ventana de error con el mensaje
	 */
	private void showErrorDialog(String mensaje) {
		JOptionPane.showMessageDialog(new JFrame(), mensaje, "Error",
				JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Habilita todos las opciones de la barra de men�
	 */
	private void enableBarElements(JMenuBar menuBar) {
		for (int itemID = 0; itemID < menuBar.getMenuCount(); itemID++) {
			menuBar.getMenu(itemID).setEnabled(true);
		}
	}

	/**
	 * habilita la opcion de jugar si el usuario existe
	 * */
	private void enableBarElementPlay(JMenuBar menuBar) {
		menuBar.getMenu(1).setEnabled(true);
		menuBar.getMenu(menuBar.getComponentCount() - 1).setEnabled(true);
	}

	/**
	 * M�todo que retorna un boolean si se puede parsear un string a integer
	 */
	private boolean tryParseInt(String value) {
		try {
			Integer.parseInt(value);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	private void selectedDisease(int x, int y) {
		Disease dis = null;
		for (int i = 0; i < game.getDiseases().size(); i++) {
			for (int j = 0; j < game.getDiseases().get(i).getEntityTiles()
					.size(); j++) {
				if (game.getDiseases().get(i).getEntityTiles().get(j).x == x
						&& game.getDiseases().get(i).getEntityTiles().get(j).y == y) {
					dis = game.getDiseases().get(i);
				}
			}
		}
		game.setLastDiseaseSelected(dis);
		
	}
}
