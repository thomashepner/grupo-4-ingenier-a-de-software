package frontend;
import java.awt.*;
import java.awt.event.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.colorchooser.*;

import backend.Game;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public class SelectDiseaseColor extends JFrame {
	private JDialog dialog;

	public SelectDiseaseColor(Image dot, Image persona, Game miGame,
			String diseaseName, int probability) {
		super("ColorChooserDemo");

		// Set up the banner at the top of the window
		final JLabel banner = new JLabel("Hola tengo hambre!", JLabel.CENTER);
		banner.setForeground(Color.yellow);
		banner.setBackground(Color.black);
		banner.setOpaque(true);
		banner.setFont(new Font("SansSerif", Font.BOLD, 24));
		banner.setPreferredSize(new Dimension(100, 65));

		JPanel bannerPanel = new JPanel(new BorderLayout());
		bannerPanel.add(banner);
		bannerPanel.setBorder(BorderFactory.createTitledBorder("Banner"));

		// Set up color chooser for setting text color
		JColorChooser tcc = new JColorChooser(
				new javax.swing.colorchooser.DefaultColorSelectionModel());
		// tcc.setChooserPanels(accp);
		AbstractColorChooserPanel[] panelsadd = new AbstractColorChooserPanel[1];
		AbstractColorChooserPanel[] panels = tcc.getChooserPanels();
		for (AbstractColorChooserPanel accp : panels) {
			if (accp.getDisplayName().equals("RGB")) {
				panelsadd[0] = accp;
				break;
				// JOptionPane.showMessageDialog(null, accp);
			}
		}
		tcc.setChooserPanels(panelsadd);
		final JLabel previewLabel = new JLabel("La entidad tendra este color",
				JLabel.CENTER);
		previewLabel.setFont(new Font("TimesRoman", Font.PLAIN, 36));
		previewLabel.setSize(previewLabel.getPreferredSize());
		previewLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 1, 0));
		tcc.setPreviewPanel(previewLabel);
		tcc.setColor(new Color(0, 0, 255));
		ImageCreator creador = new ImageCreator(tcc, dot, persona,
				miGame, diseaseName, probability);
		// crearImagenes c= new crearImagenes(tcc,gu);

		getContentPane().add(tcc);

		dialog = JColorChooser.createDialog(null, // parent comp
				"Pick A Color", // dialog title
				false, // modality
				tcc, creador, null);

		dialog.setVisible(true);
	}
	public SelectDiseaseColor(final String diseaseName, final int probability, final MainWindow m, final JPanel pan) {
		super("ColorChooserDemo");

		// Set up the banner at the top of the window
		final JLabel banner = new JLabel("Hola tengo hambre!", JLabel.CENTER);
		banner.setForeground(Color.yellow);
		banner.setBackground(Color.black);
		banner.setOpaque(true);
		banner.setFont(new Font("SansSerif", Font.BOLD, 24));
		banner.setPreferredSize(new Dimension(100, 65));

		JPanel bannerPanel = new JPanel(new BorderLayout());
		bannerPanel.add(banner);
		bannerPanel.setBorder(BorderFactory.createTitledBorder("Banner"));

		// Set up color chooser for setting text color
		final JColorChooser tcc = new JColorChooser(
				new javax.swing.colorchooser.DefaultColorSelectionModel());
		// tcc.setChooserPanels(accp);
		AbstractColorChooserPanel[] panelsadd = new AbstractColorChooserPanel[1];
		AbstractColorChooserPanel[] panels = tcc.getChooserPanels();
		for (AbstractColorChooserPanel accp : panels) {
			if (accp.getDisplayName().equals("RGB")) {
				panelsadd[0] = accp;
				break;
				// JOptionPane.showMessageDialog(null, accp);
			}
		}
		tcc.setChooserPanels(panelsadd);
		final JLabel previewLabel = new JLabel("La entidad tendra este color",
				JLabel.CENTER);
		previewLabel.setFont(new Font("TimesRoman", Font.PLAIN, 36));
		previewLabel.setSize(previewLabel.getPreferredSize());
		previewLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 1, 0));
		tcc.setPreviewPanel(previewLabel);
		tcc.setColor(new Color(0, 0, 255));
		//ImageCreator creador = new ImageCreator(tcc, dot, persona,
		//		miGame, diseaseName, probability);
		// crearImagenes c= new crearImagenes(tcc,gu);

		getContentPane().add(tcc);

		dialog = JColorChooser.createDialog(null, // parent comp
				"Pick A Color", // dialog title
				false, // modality
				tcc, new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						m.addPerson(tcc.getColor(), diseaseName, probability);
						m.addDiseasesToPanel(pan);
					}	
		
		}, null);

		dialog.setVisible(true);
	}
}
