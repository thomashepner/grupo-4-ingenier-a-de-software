package frontend;
import java.awt.*;

import javax.imageio.ImageIO;
import javax.swing.*;

import backend.Grid;

import java.awt.event.*;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Random;
import java.util.Map;

public class GridFrontend extends JPanel {
	private Grid grid;

	private int B_WIDTH;
	private int B_HEIGHT;

	private Image ballDefault;
	private Image personaSanaDefault;
	private Image personaEnfermaDefault;
	private char[][] model;
	private char[][] contagiados;
	private Map<Character,Image> mapeo;
	int mapeoActual;
	
	int defaultPersonaEnferma;




	int defaultEnfermedad;
	private float scale;

	final int tileSize = 28;

	JScrollPane scrollPane = null;

	/***
	 * Constructor de GridFrontend.
	 */
	public GridFrontend(Grid gameGrid) {
		grid = gameGrid;
		mapeoActual=35;
		this.mapeo = new HashMap<Character,Image>();
		loadImages();
		setScale(1);

		setDimensions(grid.getWidth(), grid.getHeight());

		setBorder(BorderFactory.createLineBorder(Color.black));
		setBackground(Color.black);
		setFocusable(true);
		this.requestFocus();
		
		addScrollIfNecessary();
	}

	public void addScrollIfNecessary() {
		if (B_WIDTH > 25 && B_HEIGHT > 20) {
			scrollPane = new JScrollPane(this,
					ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			scrollPane.setPreferredSize(new Dimension(
					(int) (scale * tileSize * 25),
					(int) (scale * tileSize * 20)));
		} else if (B_HEIGHT > 20) {
			scrollPane = new JScrollPane(this,
					ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setPreferredSize(new Dimension(
					(int) (scale * tileSize * 25),
					(int) (scale * tileSize * 20)));
		} else if (B_WIDTH > 25) {
			scrollPane = new JScrollPane(this,
					ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			scrollPane.setPreferredSize(new Dimension(
					(int) (scale * tileSize * 25),
					(int) (scale * tileSize * 20)));
		}
	}

	/***
	 * Recibe las dimensiones de la grilla
	 * 
	 * @param width
	 *            - El ancho o n�mero de columnas en la zona de juego
	 * @param height
	 *            - La altura o n�mero de filas en la zona de juego
	 */
	private void setDimensions(int width, int height) {
		this.B_WIDTH = width;
		this.B_HEIGHT = height;

		model = new char[width][height];
		contagiados = new char[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				model[x][y] = ' ';
				contagiados[x][y] = ' ';
			}
		}
		resize();
	}

	/***
	 * Setea el size de la grilla dependiendo la escala y el tama�o de los
	 * �conos.
	 */
	private void resize() {
		this.setPreferredSize(new Dimension((int) (scale * tileSize * B_WIDTH),
				(int) (scale * tileSize * B_HEIGHT)));
		repaint();
	}

	/***
	 * Busca las imagenes y las carga en los objetos correspondientes
	 */
	private void loadImages() {
		try {
			InputStream ballStream = this.getClass().getResourceAsStream(
					"..\\dot.png");
			InputStream personaSanaStream = this.getClass()
					.getResourceAsStream("..\\PersonaSana.png");
			InputStream personaEnfermaStream = this.getClass()
					.getResourceAsStream("..\\PersonaEnferma.png");

			ballDefault = ImageIO.read(ballStream);
			personaSanaDefault = ImageIO.read(personaSanaStream);
			personaEnfermaDefault = ImageIO.read(personaEnfermaStream);

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al cargar las imagenes");
			System.out.println(e.toString());
		}
	}

	/***
	 * Setea la escala a la que est� el mapa
	 */
	public void setScale(float scale) {
		this.scale = scale;

		try {
			loadImages();
		} catch (Exception e) {
		}
		
		ballDefault = ballDefault.getScaledInstance((int) (tileSize * scale),
				(int) (tileSize * scale), Image.SCALE_SMOOTH);
		personaSanaDefault = personaSanaDefault.getScaledInstance((int) (tileSize * scale),
				(int) (tileSize * scale), Image.SCALE_SMOOTH);
		personaEnfermaDefault = personaEnfermaDefault.getScaledInstance(
				(int) (tileSize * scale), (int) (tileSize * scale),
				Image.SCALE_SMOOTH);
				
		mapeo.put((char)mapeoActual, ballDefault);
		this.defaultEnfermedad=mapeoActual;
		mapeoActual++;
		mapeo.put((char)mapeoActual, personaSanaDefault);
		mapeoActual++;
		mapeo.put((char)mapeoActual, personaEnfermaDefault);
		this.defaultPersonaEnferma=mapeoActual;
		mapeoActual++;

		resize();
	}

	/***
	 * Es llamado al hacer repaint() y se encarga de dibujar el estado actual de
	 * la grilla
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.white);

		// g.setFont(titleFont);
		// g.setColor(labelColor);
		int i = 0;
		// Dibuja las im�genes y letras.
		for (int x = 0; x < B_WIDTH; x++) {
			for (int y = 0; y < B_HEIGHT; y++) {
				char c = model[x][y];

				// Si es una imagen dibuja la que corresponda
				if (isImage(c)) {
					g.drawImage(getImage(model[x][y]),
							(int) (scale * tileSize * x), (int) (scale
									* tileSize * y), null);
				}
				// Si no, dibuja una letra en amarillo
				else {
					System.out
							.println("Si sale esto es porque no pasa nada en .paint(Ghrapics g)"
									+ c);
				}
			}
		}
	}

	/**
	 * Traduce cada char a su correspondiente imagen
	 * 
	 * @param char - caracter a traducir
	 */
	public Image getImage(char c) {
		// Agregar todas los otros posibles �conos.
		/*
		 * Voy a dejar la cagada aca
		 */
		try{
			return this.mapeo.get(c);
		}
		catch(Exception e){
			return null;
		}
		/*
		switch (c) {
		case 'O':
			return ball;
		case ' ':
			return personaSana;
		case '#':
			return personaEnferma;

		default:
			System.out
					.println("Esto aparece en el m�todo DiplayPanel.getImage(char c)");
			return null;
		}
		*/
	}

	/**
	 * Determina si el char ingresado es un char que se puede traducir en im�gen
	 * 
	 * @param char - caracter a traducir
	 */
	private boolean isImage(char c) {
		// Deber�an ser agregados todos los caracteres. Un caracter por �cono.
		if(getImage(c)!=null)
			return true;
		return false;
	}

	private void gameOver(Graphics g) {
		String msg = "Game Over";
		Font small = new Font("Helvetica", Font.BOLD, 14);
		FontMetrics metr = getFontMetrics(small);

		g.setColor(Color.white);
		g.setFont(small);
		g.drawString(msg, (B_WIDTH - metr.stringWidth(msg)) / 2, B_HEIGHT / 2);
	}

	public char[][] getModel() {
		return this.model;
	}

	public void curar(int posx, int posy) {
		this.model[posx][posy] = ' ';
		this.contagiados[posx][posy] = ' ';
	}

	public void update(char[][] content) {
		model = content;
		repaint();
	}

	/**
	 * 
	 */
	public int getPositionX(MouseEvent e) {
		Point lugarClickeado = e.getPoint();
		return (int) Math.floor(lugarClickeado.x / scale / tileSize);
	}

	/**
	* 
	*/
	public int getPositionY(MouseEvent e) {
		Point lugarClickeado = e.getPoint();
		return (int) Math.floor(lugarClickeado.y / scale / tileSize);
	}

	public Component getFrontend() {
		if (scrollPane == null) {
			return this;
		} else {
			return scrollPane;
		}
	}
	/**
	 * Agregar Image customs
	 * @param i
	 */
	public void addImage(Image i){
		
		i = i.getScaledInstance((int) (tileSize * scale),
				(int) (tileSize * scale), Image.SCALE_SMOOTH);
		this.mapeo.put((char)this.mapeoActual,i);
		mapeoActual++;
	}
	public int getMapeoActual() {
		return mapeoActual;
	}

	public void setMapeoActual(int mapeoActual) {
		this.mapeoActual = mapeoActual;
	}
	
	public int getDefaultPersonaEnferma() {
		return defaultPersonaEnferma;
	}



	public int getDefaultEnfermedad() {
		return defaultEnfermedad;
	}

}
