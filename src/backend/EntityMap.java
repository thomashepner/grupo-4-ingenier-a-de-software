package backend;
import java.util.HashMap;
import java.util.Map;
 


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="entity")
@XmlAccessorType(XmlAccessType.FIELD)

public class EntityMap 
{
	private Map<Integer, Disease> entityMap = new HashMap<Integer, Disease>();
	 
    public Map<Integer, Disease> getEntityap() {
        return entityMap;
    }
 
    public void setEmployeeMap(Map<Integer, Disease> employeeMap) {
        this.entityMap = employeeMap;
    }

}
