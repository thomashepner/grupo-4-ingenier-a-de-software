package backend;
import java.util.Dictionary;
import java.io.Console;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Random;
import java.util.TreeMap;

public class Tile {

	private Disease disease;
	private int x, y;
	private int sickTime; // Tiempo que debe estar enfermo

	// Representa la correspondencia entre una enfermedad y si una persona es
	// inmune a ella o no.
	private Hashtable<Disease, Boolean> diseaseHash;
	private TreeMap<Integer, Disease> sickAt;

	private boolean onDisease;
	/**
	 * Char caracteristico para imprimir, default sera el color Persona sana
	 * contendra el char del ultimo
	 */
	private char imprimir;

	/*
	 * Cada Tile del tablero representa a una persona. Dos o m�s personas
	 * albergan a una enfermedad en ellas, que representa a la entidad
	 * primordial del juego
	 */
	public Tile(int x, int y) {
		this.x = x;
		this.y = y;
		this.setOnDisease(false);
		this.diseaseHash = new Hashtable<Disease, Boolean>();
		this.sickAt = new TreeMap<Integer, Disease>();
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean assignDisease(Disease disease, int actualTime) {
		synchronized (this) {
			boolean esInmune = diseaseHash.containsKey(disease);
			boolean estaEnfermo = getOnDisease();

			if (!(estaEnfermo || esInmune)) {
				return falIll(disease, actualTime);
			}
			return false;
		}
	}

	private boolean falIll(Disease disease, int actualTime) {
		// Cada enfermedad tiene una probabilidad de inmunidad dada. Si se
		// cumple la probablidad, se retorna false, luego
		// la enfermedad no infecta a esa persona.
		Random random = new Random();
		double chance = random.nextDouble();

		if (chance >= disease.getInfectionChance()) {
			return false;// No se contagia, por inmunidad
		} else {
			this.setDisease(disease);
			this.setImprimir(disease.getImprimirTile());
			this.sickTime = disease.getCure().getAcquisitionTime();
			this.setOnDisease(true);
			this.sickAt.put(actualTime, disease);
			return true;
		}
	}

	public Disease getDisease() {
		return disease;
	}

	public void setDisease(Disease disease) {
		this.disease = disease;
	}

	public boolean getOnDisease() {
		return onDisease;
	}

	public void setOnDisease(boolean onDisease) {
		this.onDisease = onDisease;

		if (!onDisease) {
			this.imprimir = (char) 36;
		}
	}

	public boolean isInmune(Disease dis) {
		if (diseaseHash.containsKey(dis)) {
			return diseaseHash.get(dis);
		}
		return false;
	}

	// Si se cumplen las condiciones, ya no est� enfermo, y no se puede enfermar
	// de nuevo de esa enfermedad. Retorna true si la curaci�n fue exitosa
	public boolean cure(Treatment cura) {
		if (disease != null && disease.isTreatable(cura)) {
			this.setOnDisease(false);
			this.imprimir = (char) 36;
			diseaseHash.put(disease, true);
			this.setDisease(null);
		}
		return !getOnDisease();
	}

	public boolean updateState(int actualTime) {
		synchronized (this) {
			if (onDisease) {
				int sickSince = sickAt.lastKey();
				if (actualTime - sickSince >= sickTime) {
					this.setOnDisease(false);
					this.imprimir = (char) 36;
					this.setDisease(null);
					return true;
				}
			}
			return false;
		}
	}

	public char getImprimir() {
		return imprimir;
	}

	public void setImprimir(char imprimir) {
		synchronized (this) {
			this.imprimir = imprimir;
		}
	}
	public String[] showState()
	{
		String[] state= new String[4];
		state[0]= "Nombre: Persona "+x+""+y;
		state[1]= "Ubicacion: ( "+ x +" , " + y + " )";
		if(disease==null)
			state[2]="Estado: Sano";
		else
			state[2]= "Estado: Enfermo con "+disease.getName();
		
		return state;
	}
}