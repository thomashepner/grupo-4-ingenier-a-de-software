package backend;
import java.util.ArrayList;
import java.io.Console;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import frontend.MainWindow;

public class Main {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				MainWindow window = new MainWindow();
				window.setVisible(true);
			}
		});
	}
}
