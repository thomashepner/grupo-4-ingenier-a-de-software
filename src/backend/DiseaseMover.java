package backend;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

public class DiseaseMover {
	private boolean leftDirection = false;
	private boolean rightDirection = true;
	private boolean upDirection = false;
	private boolean downDirection = false;

	private Disease enfermedad;
	private Grid mapa;

	public DiseaseMover(Disease desease) {
		this.enfermedad = desease;
	}

	/**
	 * Determina y realiza el siguiente movimiento
	 */
	public void move() {

		do {
			pickDirection();
		} while (!canMove());

		// Mover la entidad una posici�n:
		moveTilesOne(enfermedad.getEntityTiles());
	}

	/**
	 * Determina de forma aleatoria
	 */
	private void pickDirection() {

		// Generar int random que determinar�a a d�nde moverse:
		Random random = new Random();
		int index = random.nextInt(3);

		// L�gica de booleanos para moverse
		if (leftDirection) { // Si se habia movido antes a la izquierda, ahora
								// no puede moverse a la derecha
			leftDirection = (index == 0);

			upDirection = (index == 1);
			downDirection = (index == 2);
		} else if (rightDirection) { // Si se habia movido antes a la derecha,
										// ahora no puede moverse a la izquierda
			leftDirection = false;
			rightDirection = (index == 0);
			upDirection = (index == 1);
			downDirection = (index == 2);
		} else if (upDirection) { // Si se habia movido antes hacia arriba,
									// ahora no puede moverse hacia abajo
			leftDirection = (index == 0);
			rightDirection = (index == 1);
			upDirection = (index == 2);
			downDirection = false;
		} else if (downDirection) { // Si se habia movido antes hacia abajo,
									// ahora no puede moverse hacia arriba
			leftDirection = (index == 0);
			rightDirection = (index == 1);
			upDirection = false;
			downDirection = (index == 2);
		}
	}

	/**
	 * Desplaza todas las Tiles de una entidad en una posici�n
	 * 
	 * @return
	 */
	private void moveTilesOne(ArrayList<Point> entidadTiles) {

		// Movimiento del cuerpo
		for (int indexTileActual = entidadTiles.size() - 1; indexTileActual > 0; indexTileActual--) {

			Point actualDiseaseBodyTile = entidadTiles.get(indexTileActual);
			Point nextDiseaseBodyTile = entidadTiles.get(indexTileActual - 1);

			actualDiseaseBodyTile.setLocation(nextDiseaseBodyTile.getX(),
					nextDiseaseBodyTile.getY());
		}

		// Movimiento de la cabeza
		Point DiseaseHeadTile = entidadTiles.get(0);
		boolean enElBorde = this.coincideConBorde(DiseaseHeadTile.x,
				DiseaseHeadTile.y);

		moveHead(enElBorde, DiseaseHeadTile);

	}

	private void moveHead(boolean enElBorde, Point diseaseHead) {
		double positionX = diseaseHead.x;
		double positionY = diseaseHead.y;

		if (enElBorde) {
			if (leftDirection) {
				diseaseHead.setLocation(mapa.getWidth() - 1, positionY);
			} else if (rightDirection) {
				diseaseHead.setLocation(0, positionY);
			} else if (upDirection) {
				diseaseHead.setLocation(positionX, mapa.getHeight() - 1);
			} else if (downDirection) {
				diseaseHead.setLocation(positionX, 0);
			}
		} else {
			if (leftDirection) {
				diseaseHead.setLocation(positionX - 1, positionY);
			} else if (rightDirection) {
				diseaseHead.setLocation(positionX + 1, positionY);
			} else if (upDirection) {
				diseaseHead.setLocation(positionX, positionY - 1);
			} else if (downDirection) {
				diseaseHead.setLocation(positionX, positionY + 1);
			}
		}
	}

	/**
	 * Retorna un boolean dependiendo de si el siguiente movimiento es v�lido.
	 * 
	 * @return
	 */
	private boolean canMove() {

		boolean coincide = false;

		int headPositionX = enfermedad.getEntityTiles().get(0).x;
		int headPositionY = enfermedad.getEntityTiles().get(0).y;

		// Chequeo con personas inmunes a la enfermedad
		coincide = coincideConInmune(
				coincideConBorde(headPositionX, headPositionY), headPositionX,
				headPositionY);

		// Chequeo de choque consigo misma
		if (!coincide) {
			ArrayList<Point> futureDiseaseTiles = cloneDiseasePoints(enfermedad
					.getEntityTiles());
			moveTilesOne(futureDiseaseTiles);

			headPositionX = (int) futureDiseaseTiles.get(0).getX();
			headPositionY = (int) futureDiseaseTiles.get(0).getY();

			for (int actualTileIndex = futureDiseaseTiles.size() - 1; actualTileIndex > 0; actualTileIndex--) {

				double actualBodyTilePositionX = futureDiseaseTiles.get(
						actualTileIndex).getX();
				double actualBodyTilePositionY = futureDiseaseTiles.get(
						actualTileIndex).getY();

				boolean coincideColumna = (headPositionX == actualBodyTilePositionX);
				boolean coincideFila = (headPositionY == actualBodyTilePositionY);

				// Si se mueve horizontalmente, y est� en la misma fila, depende
				// de si
				// se est� en la misma columna.
				if ((rightDirection || leftDirection) && coincideFila) {
					coincide = coincideColumna;
					break;
				}
				// Si se mueve verticalemente, y est� en la misma columna,
				// depende de si
				// se est� en la misma fila.
				else if ((upDirection || downDirection) && coincideColumna) {
					coincide = coincideFila;
					break;
				}
			}
		}
		// Si no coincide, se puede mover.
		return !coincide;
	}

	/**
	 * Retorna un boolean dependiendo de si la posici�n de la cabeza chocar� con
	 * el borde al hacer el movimiento.
	 * 
	 * @return
	 */
	private boolean coincideConBorde(int posX, int posY) {
		if (rightDirection) {
			return (posX == mapa.getWidth() - 1);
		} else if (leftDirection) {
			return (posX == 0);
		} else if (upDirection) {
			return (posY == 0);
		} else if (downDirection) {
			return (posY == mapa.getHeight() - 1);
		}
		return false;
	}

	/**
	 * Retorna un boolean dependiendo de si la posici�n de la cabeza chocar� con
	 * una persona inmune a la enfermedad al hacer el movimiento.
	 * 
	 * @return
	 */
	private boolean coincideConInmune(boolean coincideConBorde, int posX,
			int posY) {
		// Chequeo de los m�rgenes del mapa y chequeo de persona inmune
		Tile objetivo = null;
		if (!coincideConBorde) {
			if (rightDirection) {
				objetivo = mapa.getTile(posX + 1, posY);
			} else if (leftDirection) {
				objetivo = mapa.getTile(posX - 1, posY);
			} else if (upDirection) {
				objetivo = mapa.getTile(posX, posY - 1);
			} else if (downDirection) {
				objetivo = mapa.getTile(posX, posY + 1);
			}
		} else {
			if (rightDirection) {
				objetivo = mapa.getTile(0, posY);
			} else if (leftDirection) {
				objetivo = mapa.getTile(mapa.getWidth() - 1, posY);
			} else if (upDirection) {
				objetivo = mapa.getTile(posX, mapa.getHeight() - 1);
			} else if (downDirection) {
				objetivo = mapa.getTile(posX, 0);
			}
		}
		return objetivo.isInmune(enfermedad);
	}

	public void setGrid(Grid mapa) {
		this.mapa = mapa;
	}

	public Grid getGrid() {
		return mapa;
	}

	/**
	 * Clona un conjunto de puntos de enfermedad
	 * 
	 * @return
	 */
	private ArrayList<Point> cloneDiseasePoints(ArrayList<Point> dPoints) {
		ArrayList<Point> copia = new ArrayList<>();

		for (int pointIndex = 0; pointIndex < dPoints.size(); pointIndex++) {
			Point actualPoint = dPoints.get(pointIndex);
			copia.add(new Point((int) actualPoint.getX(), (int) actualPoint
					.getY()));
		}

		return copia;
	}

	/**
	 * Retorna un string con la direcci�n en la que se est� moviendo.
	 * 
	 * @return
	 */
	private String printDirecci�n() {

		if (leftDirection)
			return "Left";
		else if (rightDirection)
			return "Right";
		else if (upDirection)
			return "Up";
		else if (downDirection)
			return "Down";

		return "None";
	}
}
