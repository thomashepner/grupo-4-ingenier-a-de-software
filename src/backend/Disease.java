package backend;
import java.util.ArrayList;
import java.util.Random;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.Serializable;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Disease implements Runnable, IColleague, Serializable {
	/* Una entidad puede tener 2..* tiles */
	private ArrayList<Point> entityTiles;// La primera es la cabeza

	private int tileNumber;// El largo de la enfermedad
	private DiseaseType type;
	private double infectionChance;
	private String name;
	private boolean alive;

	private Thread thread;
	private IMediator actuGrid;
	/**
	 * Char caracteristico de la enfermedad (representacion en frontend)
	 */
	private char imprimir;
	/**
	 * Char caracteristico de infectado
	 */
	private char imprimirTile;

	private int state; // estado de la entidad 0 inactiva, 1 activa

	private Treatment cure;

	private DiseaseMover movedor;

	private boolean pausado;

	public Disease(Point[] points, double infectionChance, String name) {
		/*
		 * La enfermedad representa una especie de "nube" infecciosa, que se
		 * mueve por el tablero y va infectando a la gente si es que puede. En
		 * caso de que una persona haya sido infectada, entonces puede contagiar
		 * por un tiempo determinado.
		 */
		pausado = false;
		alive = true;

		state = 1;
		type = new DiseaseType();
		this.infectionChance = infectionChance;
		this.name = name;
		// entityTiles es la lista de personas a las cuales est� tratando de
		// infectar la enfermedad en un momento dado.
		this.entityTiles = new ArrayList<Point>();

		for (int i = 0; i < points.length; i++) {
			this.entityTiles.add(points[i]);
		}

		cure = new Treatment();
		cure.setEffect("Cura a " + this.name);
		cure.setAcquisitionTime(10);
	}

	public Disease(double infectionChance, String name) {
		this.infectionChance = infectionChance;
		this.name = name;
		type = new DiseaseType();
	}

	// Asigna la cura de la enfermedad. Retorna true si se asigna correctamente,
	// y false si no
	// (si ya se habia asignado cura). Eventualmente podr�an ser varias curas.
	public boolean asingTreatment(Treatment cura) {
		if (cure == null) {
			cure = cura;
			return true;
		} else
			return false;
	}

	// Retorna true si la cura ingresada puede tratar la enfermedad en cuesti�n.
	public boolean isTreatable(Treatment cura) {
		return cura.getEffect().equals(cure.getEffect());
	}

	public void move() {
		// if(this.diseasesInGame.contains(this)){
		if (((Grid) actuGrid).getDiseases().contains(this)) {
			getMovedor().move();

			tryToContagiar();

			this.actuGrid.SendMessage();
		}
	}

	private boolean tryToContagiar() {
		Grid g = (Grid) actuGrid;
		Point head = this.entityTiles.get(0);
		Tile personaActual = g.getTile(head.x, head.y);

		if (personaActual.assignDisease(this,
				((Grid) actuGrid).getTiempoActual())) {
			return true;
		}

		return false;
	}

	/*
	 * public void addDiseaseList(ArrayList<Disease> diseaseList) {
	 * this.diseasesInGame = diseaseList; }
	 */

	/**
	 * Metodo que corre el thread
	 */
	int sleepe;

	public int getSleepe() {
		return sleepe;
	}

	public void setSleepe(int sleepe) {
		this.sleepe = sleepe;
	}

	@Override
	public void run() {
		System.out.println("Running " + name + " thread.");
		try {
			while (alive) {
				while (pausado) {
					try {
						Thread.sleep(100); // stop for 100ms increments
					} catch (InterruptedException ie) {
						// report or ignore
					}
				}
				move();

				Thread.sleep(sleepe);

				synchronized (this) {
					alive = live();
				}
			}
			this.erase();
		} catch (InterruptedException e) {
			System.out.println("Thread " + name + " interrupted");
		}
		System.out.println("Disease " + name + " has disappeared");
	}

	public void interrupt() {
		Thread.interrupted();
	}

	/**
	 * Metodo que crea e inicia el thread
	 */
	public void start() {
		System.out.println("Starting " + name + " thread");
		if (thread == null) {
			thread = new Thread(this, name);
			thread.start();
		}
	}

	/**
	 * Permite ver la informaci�n y el estado de una entidad
	 * 
	 * @return String con la informaci�n de la entidad
	 */
	public String[] ShowDiseaseState() {
		String[] stat = new String[6];
		stat[0] = "Nombre:   " + name;
		stat[1] = "Tama�o:   " + this.entityTiles.size();
		stat[2] = "Indice de Infecci�n:   " + infectionChance;
		stat[3] = "Tipo de enfermedad:   " + type.getType();
		stat[4] = "Medio de Contagio:	   " + type.getInfection_via();
		stat[5] = "Estado:   ";
		if (state == 0)
			stat[5] += "inactivo";
		else
			stat[5] += "activo";

		return stat;

	}

	public IMediator getActuGrid() {
		return actuGrid;
	}

	public void setActuGrid(IMediator actuGrid) {
		this.actuGrid = actuGrid;
		setMovedor(new DiseaseMover(this));
		getMovedor().setGrid((Grid) actuGrid);
	}

	public int getTileNumber() {
		return tileNumber;
	}

	public void setTileNumber(int tileNumber) {
		this.tileNumber = tileNumber;
	}

	public double getInfectionChance() {
		return infectionChance;
	}

	public void setInfectionChance(double infectionChance) {
		this.infectionChance = infectionChance;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public ArrayList<Point> getEntityTiles() {
		return entityTiles;
	}

	public void setEntityTiles(ArrayList<Point> entityTiles) {
		this.entityTiles = entityTiles;
	}

	public Grid getGrid() {
		return this.getMovedor().getGrid();
	}

	public void setGrid(Grid grid) {
		this.getMovedor().setGrid(grid);
	}

	public Treatment getCure() {
		return cure;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	@Override
	public int sendState() {

		return getState();

	}

	@Override
	public void getState(IMediator mediator) {

	}

	// genera un archivo xml Entity.xml con la entidad a enviar
	public void toxml() throws JAXBException {
		String str = "<GameOfLife>\n" + "	<Common>\n" + "		<PosX>"
				+ this.entityTiles.get(0).getX() + "</PosX>\n" + "		<PosY>"
				+ this.entityTiles.get(0).getY() + "</PosY>\n" + "		<Width>"
				+ this.tileNumber + "</Width>\n" + "		<Height>1</Height>\n"
				+ "		<OriginalGroupID>4</OriginalGroupID>\n" + "	</Common>"
				+ "	<WorldSpecific>\n" + "		<World id=\"4\">\n"
				+ "			<Disease>" + "				<Name>" + this.name + "</Name>\n"
				+ "				<tilenumber>" + this.tileNumber + "</tilenumber>\n"
				+ "				<InfectionType type=\"type\">" + this.type.getType()
				+ "</InfectionType>\n"
				+ "				<InfectionType type=\"infectionRate\">"
				+ this.type.getInfectionRate() + "</InfectionType>\n"
				+ "				<InfectionType type=\"infection_via\">"
				+ this.type.getInfection_via() + "</InfectionType>\n"
				+ "				<InfectionChance>" + this.infectionChance
				+ "</InfectionChance>\n" + "		</World>\n"
				+ "	</WorldSpecific>\n" + "</GameOfLife>\n";

		try {
			File fi = new File("Entity.xml");
			BufferedWriter output = new BufferedWriter(new FileWriter(fi));
			output.write(str);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public DiseaseMover getMovedor() {
		return movedor;
	}

	public void setMovedor(DiseaseMover movedor) {
		this.movedor = movedor;
	}

	public boolean live() {
		Grid grid = (Grid) actuGrid;
		int count = 0;
		for (int i = 0; i < grid.getWidth(); i++) {
			for (int j = 0; j < grid.getHeight(); j++) {
				if (grid.getTile(i, j).getOnDisease()) {
					if (grid.getTile(i, j).getDisease().getName()
							.compareTo(name) == 0) {
						count++;
					}
				}
			}
		}
		if (grid.getTiempoActual() <= 10)
			return true;
		if (count >= entityTiles.size() * 2 && entityTiles.size() >= 2) {
			big(grid);
			return true;
		} else if (count < entityTiles.size() * 2 && entityTiles.size() >= 2) {
			little();
			return true;
		} else {
			return false;
		}
	}

	public void big(Grid grid) {
		int size = entityTiles.size();
		Point po = new Point();
		if (size >= 2) {
			Point pf = entityTiles.get(size - 1);
			Point p2 = entityTiles.get(size - 2);
			if (pf.x == p2.x) {
				po.x = pf.x;
				if (pf.y == grid.getHeight() - 1) {
					if (pf.x == grid.getWidth() - 1) {
						po.x = pf.x - 1;
					} else {
						po.x = pf.x + 1;
					}
					po.y = pf.y;
				} else if (pf.y > p2.y) {
					po.y = pf.y + 1;
				} else if (pf.y < p2.y) {
					po.y = pf.y - 1;
				} else {
					po.y = pf.y;
				}
			} else {
				po.y = pf.y;
				if (pf.x == grid.getWidth() - 1) {
					if (pf.y == grid.getHeight() - 1) {
						po.y = pf.y - 1;
					} else {
						po.y = pf.y + 1;
					}
					po.x = pf.x;
				} else if (pf.x > p2.x) {
					po.x = pf.x + 1;
				} else if (pf.x < p2.x) {
					po.x = pf.x - 1;
				}
			}
			if (!(po.x < 0 || po.x >= grid.getWidth() || po.y < 0
					|| po.y >= grid.getHeight())) {
				entityTiles.add(po);
			}
		}
	}

	public void little() {
		int size = entityTiles.size();
		entityTiles.remove(size - 1);
	}

	public void pause() {
		pausado = true;
	}

	public void resume() {
		pausado = false;
	}

	public void erase() {
		((Grid) actuGrid).removeDiseaseFromGame(this);
	}

	public char getImprimir() {
		return imprimir;
	}

	public void setImprimir(char imprimir) {
		this.imprimir = imprimir;
	}

	public char getImprimirTile() {
		return imprimirTile;
	}

	public void setImprimirTile(char imprimirTile) {
		this.imprimirTile = imprimirTile;
	}

}