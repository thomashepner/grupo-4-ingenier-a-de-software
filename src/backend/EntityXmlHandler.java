package backend;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class EntityXmlHandler {

	public Document createStandardXML() throws Exception {
		String standardXML = "<GameOfLife>" + "<Common>" + "<PosX>value</PosX>"
				+ "<PosY>value</PosY>" + "<Width>value</Width>"
				+ "<Height>value</Height>"
				+ "<OriginalGroupID>4</OriginalGroupID>" + "</Common>"
				+ "<WorldSpecific>" + "<World id=\"4\">" + "<Disease>"
				+ "<Name>StringValue</Name>"
				+ "<InfectionChance>DoubleValue</InfectionChance>"
				+ "<CureName>StringValue</CureName>"
				+ "<AcquisitionTime>IntegerValue</AcquisitionTime>"
				+ "<CuringTime>IntegerValue</CuringTime>" + "</Disease>"
				+ "</World>" + "</WorldSpecific>" + "</GameOfLife>";
		DocumentBuilder builder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();

		InputSource source = new InputSource();
		source.setCharacterStream(new StringReader(standardXML));

		Document doc = builder.parse(source);
		return doc;
	}
	public String buildXmlString(Disease disease){
		String standardXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+"<GameOfLife>" + "<Common>" + "<PosX>"+disease.getEntityTiles().get(0).x+"</PosX>"
				+ "<PosY>"+disease.getEntityTiles().get(0).y+"</PosY>" + "<Width>1</Width>"
				+ "<Height>1</Height>"
				+ "<OriginalGroupID>4</OriginalGroupID>" + "</Common>"
				+ "<WorldSpecific>" + "<World id=\"4\">" + "<Disease>"
				+ "<Name>"+disease.getName()+"</Name>"
				+ "<InfectionChance>"+Double.toString(disease.getInfectionChance())+"</InfectionChance>"
				+ "<CureName>Cura</CureName>"
				+ "<AcquisitionTime>IntegerValue</AcquisitionTime>"
				+ "<CuringTime>IntegerValue</CuringTime>" + "</Disease>"
				+ "</World>" + "</WorldSpecific>" + "</GameOfLife>";
		return standardXML;
	}

	public NodeList GetNodeList(String ParentNode, Document doc) {
		NodeList nodeList = doc.getElementsByTagName(ParentNode);
		return nodeList;
	}

	public void GetNodeListElements(NodeList nodeList) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element element = (Element) nodeList.item(i);
			String data = getCharacterDataFromElement(element);
		}
	}

	public static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

	public Document IncomingEntity(String incomingString) throws Exception {
		DocumentBuilder builder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
		InputSource source = new InputSource();
		source.setCharacterStream(new StringReader(incomingString));

		Document incomingEntityXML = builder.parse(source);
		return incomingEntityXML;

	}

	public int getPosX(Document entityXML) {
		int posX = 0;
		Element line = null;
		NodeList nodes = entityXML.getElementsByTagName("Common");
		for (int i = 0; i < nodes.getLength(); i++) {
			Element element = (Element) nodes.item(i);
			NodeList children = element.getElementsByTagName("PosX");
			line = (Element) children.item(0);
		}
		posX = Integer.parseInt(getCharacterDataFromElement(line));
		return posX;
	}
	public int getPosY(Document entityXML) {
		int posY = 0;
		Element line = null;
		NodeList nodes = entityXML.getElementsByTagName("Common");
		for (int i = 0; i < nodes.getLength(); i++) {
			Element element = (Element) nodes.item(i);
			NodeList children = element.getElementsByTagName("PosY");
			line = (Element) children.item(0);
		}
		posY = Integer.parseInt(getCharacterDataFromElement(line));
		return posY;
	}
	public int getHeight(Document entityXML) {
		int height = 0;
		Element line = null;
		NodeList nodes = entityXML.getElementsByTagName("Common");
		for (int i = 0; i < nodes.getLength(); i++) {
			Element element = (Element) nodes.item(i);
			NodeList children = element.getElementsByTagName("Height");
			line = (Element) children.item(0);
		}
		height = Integer.parseInt(getCharacterDataFromElement(line));
		return height;
	}
	public int getWidth(Document entityXML) {
		int width = 0;
		Element line = null;
		NodeList nodes = entityXML.getElementsByTagName("Common");
		for (int i = 0; i < nodes.getLength(); i++) {
			Element element = (Element) nodes.item(i);
			NodeList children = element.getElementsByTagName("Width");
			line = (Element) children.item(0);
		}
		width = Integer.parseInt(getCharacterDataFromElement(line));
		return width;
	}
	public int getOriginalGroupID(Document entityXML) {
		int id = 0;
		Element line = null;
		NodeList nodes = entityXML.getElementsByTagName("Common");
		for (int i = 0; i < nodes.getLength(); i++) {
			Element element = (Element) nodes.item(i);
			NodeList children = element.getElementsByTagName("OriginalGroupID");
			line = (Element) children.item(0);
		}
		id = Integer.parseInt(getCharacterDataFromElement(line));
		return id;
	}
}
