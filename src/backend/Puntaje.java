package backend;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Clase destinada al calculo de puntaje para poder comprar y almacenar curas
 */
public class Puntaje {

	
	/**
	 * Score Total
	 */
	int score;
	/**
	 * Puntaje total(compras)
	 */
	private int puntaje;

	
	

	/**
	 * Ciclo actual del programa
	 */
	private int contador;

	public Puntaje() {
		this.score=0;
		this.puntaje = 0;
		this.contador = 0;

	}

	




	
	


	public int getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(int puntaje) {
		this.puntaje = puntaje;
	}

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}


}