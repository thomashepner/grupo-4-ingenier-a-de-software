package backend;
import java.awt.Point;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import java.util.ArrayList;
import java.util.Random;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import frontend.GridFrontend;

public class Grid implements IMediator {
	private Tile[][] tiles;
	private ArrayList<Disease> diseases;
	private int width, height;
	ArrayList<IColleague> ColleagueList;

	private GridFrontend gridFron;

	private int tiempoActual;

	private char[][] model;

	public Grid(int width, int height, ArrayList<Disease> diseases) {
		/*
		 * Primero se construye el tablero, con la cantidad correcta de bloques,
		 * dependiendo del ancho y del alto
		 */
		this.model = new char[width][height];
		this.setWidth(width);
		this.setHeight(height);
		this.tiles = new Tile[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				tiles[i][j] = new Tile(i, j);
				tiles[i][j].setImprimir((char) 36);
			}
		}
		this.diseases = diseases;
		ColleagueList = new ArrayList<IColleague>();

	}

	public GridFrontend getGridFron() {
		return gridFron;
	}

	public void setGridFron(GridFrontend gridFron) {
		this.gridFron = gridFron;
	}

	public void addEntities(Disease newDiseases) {
		this.diseases.add(newDiseases);
	}

	public Tile[][] getTiles() {
		return tiles;
	}

	public Tile getTile(int posX, int posY) {
		return tiles[posX][posY];
	}

	public void setTiles(Tile[][] tiles) {
		this.tiles = tiles;
	}

	public ArrayList<Disease> getDiseases() {
		return diseases;
	}

	public void setDiseases(ArrayList<Disease> diseases) {
		this.diseases = diseases;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getTiempoActual() {
		return tiempoActual;
	}

	/**
	 * Metodo que transmite la enfermedad de una persona a otra que se encuentre
	 * a su lado si es que esta no se encuentra enferma, la enfermedad que se
	 * transmita primero a la persona es la que se mantiene
	 */
	public void transmitDisease(int segundosTranscurridos) {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Tile actual = tiles[i][j];
				if (actual.getOnDisease()
						&& spreadDisease(i, j, segundosTranscurridos)) {
					SendMessage();
				}
			}
		}
	}

	public boolean spreadDisease(int i, int j, int segundosTranscurridos) {
		double infection_chance = tiles[i][j].getDisease().getInfectionChance();
		double chance = generateChance();

		// este numero indica en que direccion debe infectar: 0
		// arriba, 1 derecha, 2 abajo y 3 izquierda
		int direction_to_infect = generateDirection();
		int up = 0, down = 2, left = 3, right = 1;
		boolean infect = infection_chance >= chance;

		boolean success = false;

		if (infect && possible_to_infect(i, j, direction_to_infect)) {
			if (direction_to_infect == up) {
				success = tiles[i][j - 1].assignDisease(
						tiles[i][j].getDisease(), segundosTranscurridos);
			} else if (direction_to_infect == down) {
				success = tiles[i][j + 1].assignDisease(
						tiles[i][j].getDisease(), segundosTranscurridos);
			} else if (direction_to_infect == left) {
				success = tiles[i - 1][j].assignDisease(
						tiles[i][j].getDisease(), segundosTranscurridos);
			} else if (direction_to_infect == right) {
				success = tiles[i + 1][j].assignDisease(
						tiles[i][j].getDisease(), segundosTranscurridos);
			}
		}
		return success;
	}

	public double generateChance() {
		Random random = new Random();
		double chance = random.nextDouble();
		return chance;
	}

	public int generateDirection() {
		Random random = new Random();
		int direction_to_infect = random.nextInt(4);
		return direction_to_infect;
	}

	public boolean possible_to_infect(int i, int j, int dir) {
		boolean pos = false;
		if (dir == 0) {
			pos = j - 1 >= 0 && !tiles[i][j - 1].getOnDisease();
		} else if (dir == 2) {
			pos = j + 1 < height && !tiles[i][j + 1].getOnDisease();
		} else if (dir == 1) {
			pos = i + 1 < width && !tiles[i + 1][j].getOnDisease();
		}
		if (dir == 3) {
			pos = i - 1 >= 0 && !tiles[i - 1][j].getOnDisease();
		}
		return pos;
	}

	@Override
	/**
	 * Todos los thread deben ir suscrito a este metodo
	 * Es un evento que nos permite actualizar la 
	 */
	public void SendMessage() {
		synchronized (this) {
			model = new char[width][height];

			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					model[i][j] = tiles[i][j].getImprimir();
				}
			}

			for (int i = 0; i < this.diseases.size(); i++) {
				updateDiseaseInMap(diseases.get(i));
			}
			this.gridFron.update(model);
		}
	}

	private void updateDiseaseInMap(Disease disease) {
		synchronized (disease) {
			ArrayList<Point> arr = disease.getEntityTiles();
			for (int j = 0; j < arr.size(); j++) {
				int coordX = arr.get(j).x;
				int coordY = arr.get(j).y;
				if (coordX < 0 || coordY >= this.getWidth() || coordY < 0
						|| coordY >= this.getHeight())
					throw new ArrayIndexOutOfBoundsException(disease.getName()+" "+j + "/"
							+ arr.size() + "||" + "x: " + coordX + ", y: "
							+ coordY);
				model[coordX][coordY] = disease.getImprimir();
			}
		}
	}

	public void CureTile(int x, int y, Treatment cure) {
		/*
		 * if (puntaje.buyTreatment(cure) &&
		 * cure.getDiseases().contains(tiles[x][y].getDisease())) {
		 * tiles[x][y].cure(cure); }
		 */
		if (tiles[x][y].cure(cure)) {
			SendMessage();
		}
	}

	public void updateState(int segundosTranscurridos) {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (tiles[i][j].updateState(segundosTranscurridos)) {
					SendMessage();
				}
			}
		}
	}

	/**
	 * Genera una nueva enfermedad cuando existe una gran acumulacion de tiles
	 * infectados, revisa para cada enfermedad.
	 * **/
	public void GenerateDisease() {
		int n = 3;// porque yo lo digo (Caro) xD
		for (int i = 0; i < diseases.size(); i++) {
			CheckDisease(n, diseases.get(i));
		}
	}

	/**
	 * Revisa si existe un cuadrado de tiles de tama�o n en la posicion (i,j)
	 * infectado con esa enfermedad
	 * **/
	public boolean checkSquareTiles(int i, int j, int n, Disease dis) {
		boolean posible = false;

		if (i - n < 0)
			return false;
		if (j - n < 0)
			return false;
		for (int k = i - n; k < n; k++) {
			posible = true;
			for (int l = j - n; l < n; l++) {
				if (tiles[k][l].getDisease().getName().compareTo(dis.getName()) != 0) {
					posible = false;
				}
			}
		}
		return posible;
	}

	/**
	 * Revisa si en la grid existe un cuadrado de tama�o n infectado con la
	 * enfermedad
	 * **/
	public void CheckDisease(int n, Disease dis) {
		for (int i = n; i < this.width; i++) {
			for (int j = n; j < this.height; j++) {
				if (checkSquareTiles(i, j, n, dis)) {
					Point[] num = new Point[3];
					num[0] = new Point(tiles[i - n][j].getX(),
							tiles[i - n][j].getY());
					num[1] = new Point(tiles[i - n + 1][j].getX(),
							tiles[i - n][j].getY());
					num[2] = new Point(tiles[i - n + 2][j].getX(),
							tiles[i - n][j].getY());

					Disease disea = new Disease(num, dis.getInfectionChance(),
							dis.getName());

					this.addEntities(disea);
					i = i + n;
					j = j + n;
				}
			}
		}

	}

	public void setTiempoActual(int segundosTranscurridos) {
		tiempoActual = segundosTranscurridos;
	}

	public boolean tooSick() {
		int count = 0;
		for (int i = 0; i < this.width; i++) {
			for (int j = 0; j < this.height; j++) {
				if (this.tiles[i][j].getOnDisease())
					count++;
			}

		}
		if (count > 0.95 * width * height)
			return true;
		return false;
	}

	public void ActStat() {
		// TODO Auto-generated method stub
	}

	public void removeDiseaseFromGame(Disease disease) {
		synchronized (this) {
			disease.getEntityTiles().clear();
			diseases.remove(disease);
			SendMessage();
			System.out.println("A diseases has died. " + diseases.size()
					+ " remaining");
		}
	}
}
