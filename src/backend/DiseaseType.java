package backend;
import java.util.Random;


public class DiseaseType 
{
	private String type;
	private double infectionRate;
	private String infection_via;
	
	
	public DiseaseType ()
	{
		this.type=randomType(); //tipo, se refiere a si es una inflamacion, un virus, un hongo...
		infectionRate= randomInfectionRate();
		infection_via= randomInfectionVia();
		
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getInfectionRate() {
		return infectionRate;
	}
	public void setInfectionRate(double infectionRate) {
		this.infectionRate = infectionRate;
	}
	public String getInfection_via() {
		return infection_via;
	}
	public void setInfection_via(String infection_via) {
		this.infection_via = infection_via;
	}
	public String randomType()
	{
		Random random=new Random();
		int numero=random.nextInt(4);
		if(numero==0)return "Hongo";
		else if(numero==1)return "Virus";
		else if(numero==2)return "Bacteria";
		else if(numero==3)return "Autoinmune";
		return "Desconocido";
	}
	
	public String randomInfectionVia()
	{
		Random random=new Random();
		int numero=random.nextInt(3);
		if(numero==0)return "Oral";
		else if(numero==1)return "Aerea";
		else if(numero==2)return "Sexual";
		return "Sangre";
	}
	
	public double randomInfectionRate()
	{
		Random random=new Random();
		Double numero=random.nextDouble();
		return numero;
	}
	
}
