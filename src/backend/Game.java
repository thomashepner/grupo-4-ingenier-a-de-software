package backend;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import frontend.GridFrontend;

public class Game implements ActionListener, Runnable {
	private Grid grid;
	private GridFrontend frontend;
	private ArrayList<Disease> diseases;
	private Thread thread;
	private Puntaje puntaje;
	int contador;
	
	private Disease lastDiseaseSelected;
	private boolean gameover;

	

	private boolean pausado;

	public Game() {
		diseases = new ArrayList<Disease>();
		contador = 0;
		pausado = false;
		gameover = false;
		puntaje = new Puntaje();
		puntaje.setPuntaje(1000);
		createDefaultDesease();
	}

	public void setMapDimentions(int gridWidth, int gridHeigth) {
		if (gridWidth == -1 || gridHeigth == -1) {
			gridWidth = 800;
			gridHeigth = 800;
		}
		grid = new Grid(gridWidth, gridHeigth, diseases);
		frontend = new GridFrontend(grid);
		grid.setGridFron(frontend);
	}

	/**
	 * Inicializa la posici�n de la enfermedad Default en el centro, y comienza
	 * el thread.
	 */
	public void init() {
		start();
		for (int indexD = 0; indexD < diseases.size(); indexD++) {
			diseases.get(indexD).start();
		}
	}

	private void createDefaultDesease() {
		Point[] crearomo = new Point[4];
		crearomo[0] = new Point(0, 1);
		crearomo[1] = new Point(0, 2);
		crearomo[2] = new Point(0, 3);
		crearomo[3] = new Point(0, 4);

		Disease dd = new Disease(crearomo, 0.20, "Pabf");
		dd.setImprimir((char) 35);
		dd.setImprimirTile((char) 37);
		dd.setSleepe(400);

		diseases.add(dd);
	}

	public void setInitialDiseases() {
		int gridWidth = grid.getWidth();
		int gridHeigth = grid.getHeight();

		for (int indexD = 0; indexD < diseases.size(); indexD++) {
			Disease d = diseases.get(indexD);
			d.setActuGrid(grid);
		}
	}

	public Disease addNewDisease(String name, int largo, int proba,
			char disease, char tile) {
		Point[] points = new Point[largo];

		for (int index = 0; index < largo; index++) {
			points[index] = new Point(10 / 2, 10 / 2 + index);
		}
		Disease d = new Disease(points, proba / 100.0, name);
		d.setImprimir(disease);
		d.setImprimirTile(tile);
		puntaje.score += 200;
		puntaje.setPuntaje(puntaje.getPuntaje() + 200);
		d.setSleepe(350);
		diseases.add(d);
		return d;
	}

	public void addNewDiseaseInGame(String diseaseName, int i, int proba,
			char disease, char tile) {
		synchronized (grid) {
			Disease d = addNewDisease(diseaseName, i, proba, disease, tile);
			d.setActuGrid(grid);
			startDisease(d);
			System.out.println("Added new disease to Game " + d.getName());
		}
	}

	public void startDisease(Disease desease) {
		resume();
		desease.start();
	}

	public Grid getGrid() {
		return grid;
	}

	public GridFrontend getGridFrontend() {
		return frontend;
	}

	public String[] getAvailableCures() {
		String[] curas = new String[diseases.size()];

		for (int indexD = 0; indexD < diseases.size(); indexD++) {
			// curas[indexD] = diseases.get(indexD).getCure();
			curas[indexD] = "Cura de " + diseases.get(indexD).getName();
		}

		return curas;
	}

	public boolean cure(int xValue, int yValue, String diseaseName) {
		Disease disease = null;
		diseaseName = diseaseName.substring(8);

		for (int indexD = 0; indexD < diseases.size(); indexD++) {
			if (diseases.get(indexD).getName().equals(diseaseName)) {
				disease = diseases.get(indexD);
			}
		}
		if (puntaje.getPuntaje() < 10) {
			return false;
		}
		if (disease == null) {
			puntaje.score -= 30;
			puntaje.setPuntaje(puntaje.getPuntaje() - 10);
			return false;
		} else {
			if (grid.getTile(xValue, yValue).getOnDisease()) {
				if (grid.getTile(xValue, yValue).getDisease().getName()
						.compareTo(disease.getName()) == 0) {
					puntaje.score += 300;
				}
			}
			puntaje.setPuntaje(puntaje.getPuntaje() - 10);
			grid.CureTile(xValue, yValue, disease.getCure());
			return true;
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void run() {
		System.out.println("Running game thread.");
		try {
			while (!gameover) {

				while (pausado) {
					try {
						Thread.sleep(100); // stop for 100ms increments
					} catch (InterruptedException ie) {
						// report or ignore
					}
				}

				grid.setTiempoActual(contador);
				grid.updateState(contador);
				grid.transmitDisease(contador);
				//checkDiseasesState();
				//grid.GenerateDisease();
				puntaje.score += 30;
				puntaje.setPuntaje(puntaje.getPuntaje() + 20);

				Thread.sleep(1000);
				contador++;

				gameover = diseases.size() == 0 || grid.tooSick();
			}
		} catch (InterruptedException e) {
			System.out.println("Thread game interrupted");
		}
		System.out.println("Game has ended");
	}

	/**
	 * Metodo que crea e inicia el thread
	 */
	public void start() {
		System.out.println("Starting game thread");
		if (thread == null) {
			thread = new Thread(this, "Game");
			thread.start();
		}
	}

	public void pause() {
		pausar();
		for (int indexD = 0; indexD < diseases.size(); indexD++) {
			diseases.get(indexD).pause();
		}
	}

	public void resume() {
		resumir();
		for (int indexD = 0; indexD < diseases.size(); indexD++) {
			diseases.get(indexD).resume();
		}
	}

	private void pausar() {
		pausado = true;
	}

	private void resumir() {
		pausado = false;
	}

	public ArrayList<Disease> getDiseases() {
		return diseases;
	}

	public void checkDiseasesState() {
		for (int i = 0; i < diseases.size(); i++) {

			if (diseases.get(i).getEntityTiles().size() == 0) {
				// System.out.println("Murio: "+diseases.get(i).getName());
				diseases.remove(i);
				puntaje.setPuntaje(puntaje.getPuntaje() - 1000);
				puntaje.score += 500;
			} else {
				// System.out.println("Queda: "+diseases.get(i).getName());
			}
		}
	}
	public Disease getLastDiseaseSelected() {
		return lastDiseaseSelected;
	}

	public void setLastDiseaseSelected(Disease lastDiseaseSelected) {
		this.lastDiseaseSelected = lastDiseaseSelected;
	}
	public boolean isGameover() {
		return gameover;
	}
	public Puntaje getPuntaje() {
		return puntaje;
	}

	public void setPuntaje(Puntaje puntaje) {
		this.puntaje = puntaje;
	}

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}
}
