package backend;
import java.sql.*;
import org.sqlite.SQLite;
public class User {
	/**
	 * Ubicacion de la base de datos
	 */
	private String dataUbication;
	private String usuar="";
	private boolean savedScore=false;
	public User(){
		dataUbication="src\\Base De datos\\usuarioSoftware.db";
		//Base de datos local 
		
	}
	
	 public String getUsuario()
	{
		return usuar;
	}
	
	 public boolean scoreIsSaved()
	 {
		 return savedScore;
	 }
	 
	 /** Nos permite saber si un usuario se puede conectar con los datos dados
	 * @param user Corresponde al usuario
	 * @param password	corresponde a su contrase�a
	 * @return un bool que dice si se puede conectar o no el usuario
	 */
	
	public boolean canConnect(String user, String password){
		boolean retorno=false;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");

			c = DriverManager.getConnection("jdbc:sqlite:"+dataUbication);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql = "SELECT * FROM usuario"; 
			ResultSet r =stmt.executeQuery(sql);
			while(r.next())
			{			
				if(r.getString("username").equals(user) && r.getString("password").equals(password))
				{
					usuar=user;
					retorno=true;
				}
			}
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		return retorno;
	} 
	/**
	 * A�adir usuario
	 * @param username Nombre de usuario
	 * @param password Contrase�a
	 * @param curso Curso al que pertenece
	 * @param name Nombre de alumno
	 */
	public void addUser(String username, String password, String curso, String name)
	{
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");

			c = DriverManager.getConnection("jdbc:sqlite:"+dataUbication);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql ="INSERT INTO usuario (username,password,name,curso) " +
			"Values ("+username+","+password+","+curso+","+name+")";
			stmt.executeUpdate(sql);
			
			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		
	}
	/**
	 * A�adir puntaje a la base de datos
	 * @param user Usuario que obtubo este puntaje
	 * @param score Puntaje obtenido
	 */
	public void addPuntaje(String user, int score){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");

			c = DriverManager.getConnection("jdbc:sqlite:"+dataUbication);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql ="INSERT INTO score (user, sco) " +
			"Values (\""+user+"\","+score+")";
			stmt.executeUpdate(sql);
			savedScore=true;
			stmt.close();
			//c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}
	/**
	 * Devuelve los 10 mejores puntajes 
	 * @return arreglo de string con los 10 mejores jugadores.
	 */
	public String[][] topten(){
		String[][] retorno = new String[10][2];
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");

			c = DriverManager.getConnection("jdbc:sqlite:"+dataUbication);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql = "SELECT * from score ORDER BY sco ASC LIMIT 10";
			
			ResultSet r =stmt.executeQuery(sql);
			int count =0;
			//		if(r.getString("username")==user && r.getString("password")==password)
			while(r.next())
			{
				//retorno[count]=count+".-"+"User : "+r.getString("user")+" Score: "+r.getString("sco");
				retorno[count][0]=r.getString("user");
				retorno[count][1]=r.getString("sco");
				count++;
			}
			
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		this.orderRanking(retorno);
		return retorno;
	}
	
	/**
	 * Devuelve los 10 mejores puntajes locales (del curso)
	 * @return arreglo de string con los 10 mejores jugadores del curso.
	 */
	public String[][] toptenlocal(){
		String[][] retorno = new String[10][2];
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");

			c = DriverManager.getConnection("jdbc:sqlite:"+dataUbication);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql = "SELECT * from score WHERE user IS \""+usuar+"\" order by sco DESC limit 10";
			
			ResultSet r =stmt.executeQuery(sql);
			int count =0;
			//		if(r.getString("username")==user && r.getString("password")==password)
			while(r.next())
			{
				//retorno[count]=count+".-"+"User : "+r.getString("user")+" Score: "+r.getString("sco");
				retorno[count][0]=r.getString("user");
				retorno[count][1]=r.getString("sco");
				count++;
			}
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		orderRanking(retorno);
		return retorno ;
	}
	
	public void orderRanking(String[][] ranking)
	{
		int j;
		boolean flag = true;   // set flag to true to begin first pass
		int tempI;   //holding variable
		String tempS;
		while ( flag )
		{
			flag= false;    //set flag to false awaiting a possible swap
			for( j=0;  j < 9;  j++ )
		    {
				if(ranking[j][1]==null ||ranking[j+1][1]==null)
					continue;
				if ( Integer.parseInt(ranking[ j ][1]) <  Integer.parseInt(ranking[j+1][1]) )   // change to > for ascending sort
				{
					tempI =  Integer.parseInt(ranking[ j ][1]);                //swap elements
		            tempS=ranking[j][0];
		            ranking[ j ][1] = ranking[ j+1 ][1];
		            ranking[j][0]= ranking[j+1][0];
		            ranking[ j+1 ][1] = ""+tempI;
		            ranking[j+1][0]=tempS;
		            flag = true;              //shows a swap occurred  
		            } 
		      } 
		} 
	}

}
