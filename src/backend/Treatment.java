package backend;
import java.util.ArrayList;

public class Treatment {
	private int effectDuration;
	private int acquisitionTime;
	private String efecto;
	private ArrayList<Disease> diseases;

	public Treatment() {
		diseases = new ArrayList<Disease>();
	}

	public int getEffectDuration() {
		return effectDuration;
	}

	public void setEffectDuration(int effectDuration) {
		this.effectDuration = effectDuration;
	}

	public int getAcquisitionTime() {
		return acquisitionTime;
	}

	public void setAcquisitionTime(int acquisitionTime) {
		this.acquisitionTime = acquisitionTime;
	}

	public ArrayList<Disease> getDiseases() {
		return diseases;
	}

	public void setDiseases(ArrayList<Disease> diseases) {
		this.diseases = diseases;
		for (int i = 0; i < diseases.size(); i++) {
			diseases.get(i).asingTreatment(this);
		}
	}

	public String getEffect() {
		return efecto;
	}

	public void setEffect(String efecto) {
		this.efecto = efecto;
	}

	public String activateEffect() {
		return getEffect();
	}

}
