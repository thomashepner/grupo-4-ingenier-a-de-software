README LifeDisease - 15/5/2014

Consideraciones acerca del código:
El programa funciona de la siguiente manera: la entidad es una nubue infecciosa, similar a una "culebra" que se va moviendo por el 
tablero. Esto representa que la enfermedad va pasando por donde están las personas y las va tratando de infectar. La entidad (enfermedad)
tiene una lista de bloques por donde está pasando en ese momento, que va variando conforme se va moviendo. La dirección en la que viaja la 
enfermedad es aleatoria, y puede moverse para todos lados (izquierda, derecha, arriba y abajo), pero, al igual que un "snake", no puede chocar
consigo misma.
Las reglas de funcionamiento de la entidad son las siguientes:
-> La enfermedad se mueve de forma aleatoria, de la misma forma que un "snake", sin capacidad de chocar consigo misma ni atravesar los límites
del tablero.
-> Dependiendo de las características de la enfermedad, esta intentará infectar a cada una de las personas por las que está pasando. Por ejemplo,
en caso de ser una gripe, habrá una probabilidad aproximada de un 5% de infectar a cada una de las personas en las que está ubicada en ese momento.
-> Si una persona es infectada comenzará a infectar a los bloques adyacentes (personas que están cerca), con la misma probabilidad correspondiente
a la enfermedad que padece. 
-> Dependiendo de la cantidad de personas infectadas en todo el tablero será el largo de la entidad-enfermedad. O sea, la existencia de la entidad 
está condicionada al númmero de personas que ha infectado durante todo el juego. Aprovechando esta característica es que el usuario puede usar curas
para erradicar la enfermedad, curando personas infectadas y disminuyendo el largo de la entidad.
-> De momento una persona no puede padecer de dos o más enfermedades a la vez (complicación a añadir en versiones futuras).
-> Las curas se aplican en una persona infectada en específico, evitando que estas contagien a sus vecinos.
-> Existe una correspondencia única entre enfermedad y cura, o sea, una enfermedad tiene una sola cura, y una cura sirve para solo una enfermedad.
-> Una vez que una persona padeció de una enfermedad no puede volver a contagiarse de esta. 
-> Dependiendo de la enfermedad será la cantidad de turnos en que una persona infectada estará contagiándola. Por ejemplo, una vez que una persona
se enferma de gripe, pasan 5 turnos hasta que deja de contagiar.

Update 27/6/2014
Funcionamiento de migración de entidades: al enviar una enfermedad se selecciona un mundo hacia donde mandarlo y cual de todas las enfermedades en juego se debe mandar. Luego se borra dicha enfermedad del frontend y se elimina de las listas en el backend, se crea un string xml para enviar la enfermedad, y se envía.
Para recibir enfermedades, simplemente se recibe el xml y se toman las dimensiones de la entidad. Luego se crea una enfermedad en el centro del tablero (estilo de portal), con el tamaño indicado en el xml. Actualmente se crea una enfermedad estándar para ilustrar el proceso, pero simplemente porque hay un leve error en el casteo de documentos que no alcanzamos a arreglar antes de la entrega. Sin embargo esto es fácil de corregir, ya que no tiene que ver con el Networking.

Update 1/7/2014
Está lista la migración de entidades desde el programa hacia otros mundos. Se logra seleccionando el menú Conexión > Enviar entidad, y seleccionando un mundo y una entidad. Una vez que se presiona el botón "Enviar", pareciera como que el programa se quedara pegado, lo cual NO ES ASÍ. Simplemente espera a hacer conexión con otro mundo por un tiempo determinado (algo así como 30 segundos), y en caso de no lograrlo, muestra una ventana que dice el tipo de error, y continúa el juego normalmente.